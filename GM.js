/**
 * This method performs a similar function to the standard XMLHttpRequest object, but allows these requests to cross the same origin policy boundaries.
 * @param {Object} details
 * @see <a href="http://wiki.greasespot.net/GM_xmlhttpRequest">http://wiki.greasespot.net/GM_xmlhttpRequest#Arguments</a>
 * @return XMLHttpRequest
 */
function GM_xmlhttpRequest(details ) {}


/**
 * <p>This API object allows a User script to access "custom" properties--variable and functions defined in the page--set by the web page. The unsafeWindow object is shorthand for window.wrappedJSObject. It is the raw window object inside the XPCNativeWrapper provided by the Greasemonkey sandbox.</p>
 * <strong>USE OF UNSAFEWINDOW IS INSECURE, AND IT SHOULD BE AVOIDED WHENEVER POSSIBLE.</strong>
 * <p>unsafeWindow bypasses Greasemonkey's XPCNativeWrapper-based security model, which exists to make sure that malicious web pages cannot alter objects in such a way as to make greasemonkey scripts (which execute with more privileges than ordinary JavaScript running in a web page) do things that their authors or users did not intend. User scripts should therefore avoid calling or in any other way depending on any properties on unsafeWindow - especially if they are executed for arbitrary web pages, such as those with &#x40;include *, where the page authors may have subverted the environment in this way.
 * <p>User script authors are <b>strongly</b> encouraged to learn how XPCNativeWrappers work, and how to perform the desired function within their security context, instead of using unsafeWindow to break out.</p>
 * <p><em>BUG: In Firefox 3.0 the prototype field will always be undefined for objects accessed through unsafeWindow. The techniques in <a href="http://wiki.greasespot.net/Category:Coding_Tips:Interacting_With_The_Page">Category:Coding Tips:Interacting With The Page</a> can work around this problem.</em></p>
 */
var /**@type Window*/ unsafeWindow = new Object();

/**
 * This method deletes an existing name / value pair from storage.
 * @since 0.3b
 * @param {String} name The property name to set.
 * @return undefined
 */
function GM_deleteValue(name) {return undefined;}


/**
 * This method retrieves a value that was set with GM_setValue. See GM_setValue for details on the storage of these values.
 * @param {String} name The property name to get. See GM_setValue for details.
 * @param {String|Number|Boolean} [defaultValue=undefined]
 * @return <ul><li>When this name has been set String, Integer or Boolean as previously set</li><li>When this name has not been set, and default is provided The value passed as default</li><li>When this name has not been set, and default is not provided undefined</li></ul>
 */
function GM_getValue( name, defaultValue ){return 1|""|true|undefined;}

/**
 * This method allows user script authors to persist simple values across page-loads.<br/>
 * Strings, booleans, and integers are currently the only allowed data types. Values are<br/>
 * saved in the Firefox preferences back end and can be manually inspected or changed by typing<br/>
 * about:config  in the address bar and searching for the preference <br/>
 * name "greasemonkey.scriptvals.namespace/script_name.value_name".<br/>
 * (For appropriate values of namespace, script_name and value_name.)<br/>
 * The Firefox preference store is not designed for storing large amounts of data. <br/>
 * There are no hard limits, but very large amounts of data may cause Firefox to consume more <br/>
 * memory and/or run more slowly. [1] Integer preferences must be in the range -231-1 to 231-1. <br/>
 * Numbers outside this range can be stored by using .toString() before GM_setValue. <br/>
 * @since 0.3b
 * @param {String} name The unique (within this script) name for this value. Should be restricted to valid Javascript identifier characters.
 * @param {Number|Boolean|String} value String/Integer/Boolean: Any valid value of these types. Any other type may cause undefined behavior, including crashes.
 * @return undefined
*/
function GM_setValue( name, value ){return undefined;}

/**
 * This method retrieves an array of preference names that this script has stored.<br/>
 * See GM_setValue for details on the storage of these values.
 * @return Array.<String>
 */
function GM_listValues(){return new Array();}

/**
 * This method adds a string of CSS to the document. It creates a new &lt;style&gt; element, adds the given CSS to it, and inserts it into the &lt;head&gt;.
 * @param {String} css A string of CSS. 
 * @return undefined
 */
function GM_addStyle( css ) {return undefined;}

/**
 * Given a defined &#x40;resource, this method returns it as a string.
 * @param {String} resourceName The name provided when the @resource was defined, follow that link for valid naming restrictions.
 * @return String
 * @throws Throws an Error when the named resource does not exist.
 */
function GM_getResourceText( resourceName ) {return "a\nb\nabc1.0.0.1cba\na\nd";}

/**
 * Given a defined &#x40;resource, this method returns it as a string.
 * @param {String} resourceName The name provided when the @resource was defined, follow that link for valid naming restrictions.
 * @return String greasemonkey-script:[script uuid]/[resource name]<br/>i.e. greasemonkey-script:94242686-1400-4dce-982a-090cbfef7ba1/image<br/><b>Prior to Greasemonkey 1.0:</b> Returns string containing a base64 encoded data: URI. <br/><em>Note: This can be used anywhere a data: URI will work (E.G. &lt;img&gt; or &lt;script&gt;). This does not include &lt;object&gt; or &lt;embed&gt;.<br/>The result is a base64 encoded URI, which is then also URI encoded, as suggested by Wikipedia, because of "+" and "/" characters in the base64 alphabet. Thus, for certain usage, this URI encoding may need to be removed.</em>
 * @throws Throws an Error when the named resource does not exist.
 */
function GM_getResourceURL( resourceName ) {return "a\nb\nabc1.0.0.1cba\na\nd";}

/**
 * This method opens the specified URL in a new tab. As of Greasemonkey 0.8.2 it obeys the built in "When I open a link in a new tab, switch to it immediately" preference (browser.tabs.loadInBackground).
 * @param {String} url
 * @return {Window} the window object created.
 */
function GM_openInTab(url) {return null;}

/**
 * Sets the current contents of the operating system's clipboard.
 * @param text any text
 * @return undefined
 */
function GM_setClipboard(text) {return undefined;}


/**
 * An object that exposes various information about Greasemonkey and the running User Script.
 */
var /**@type Object*/ GM_info = {};

/**
 * A string, the entire literal Metadata Block (without the delimiters) for the currently running script.
 */
GM_info.scriptMetaStr = "";

/**
 * A boolean; when true Greasemonkey will attempt to auto-update the script.
 */
GM_info.scriptWillUpdate = false;

/**
 * The version of Greasemonkey, a string e.g. 0.9.16.
 */
GM_info.version = "1.0";

/**
 * An object containing data about the currently running script. See more detail below.
 */
GM_info.script = {};

/**
 * Possibly empty string.
 */
GM_info.script.description = "";

/**
 * Possibly empty Array of strings.
 */
GM_info.script.excludes = new Array();

/**
 * Possibly empty Array of strings. 
 */
GM_info.script.includes = new Array();

/**
 * Possibly empty Array of strings. (Bug in 0.9.16.)
 */
GM_info.script.matches = new Array();

/**
 * An object whose keys are resource names and values are the corresponding URLs. (As of Greasemonkey 1.2.)
 */
GM_info.script.resources = new Object();

/**
 * String
 */
GM_info.script.name = "";

/**
 * Possibly empty string.
 */
GM_info.script.namespace = "";

/**
 * String
 */
GM_info.script['run-at'] = "";

/**
 * Boolean
 */
GM_info.script.unwrap = false;

/**
 * Possibly empty string.
 */
GM_info.script.version = "";



// ==UserScript==
// @name		OGame NX
// @description	Enhances OGame with useful additions
// @namespace	https://bitbucket.org/YamiNoSensei/ogame-nx/wiki/Home
// @downloadURL https://userscripts.org/scripts/source/172518.user.js
// @updateURL	https://userscripts.org/scripts/source/172518.user.js
// @resource	ONX_CUR_SCRIPT https://userscripts.org/scripts/source/172518.user.js
// @version		1.0.0.0
// @run-at 		document-end
// @copyright	2012+, YamiNoSensei
// @license		GNU Lesser General Public License (LGPL)
// @require		http://code.jquery.com/jquery-2.0.3.min.js
// @include		http://*.ogame.*/game/index.php*
// @grant       GM_getValue
// @grant		GM_setValue
// @grant		GM_listValues
// @grant		GM_deleteValue
// @grant		GM_getResourceText
// @grant		GM_getResource
// @grant		GM_setClipboard
// @grant		unsafeWindow
// ==/UserScript==

'use strict';

if (navigator.userAgent.search('Firefox/') > -1) {
	this.$ = this.jQuery = jQuery.noConflict(true);
}

/**
 * Boots up OGame NX
 */
$(document).ready(function() {
	try {
		//Startup
		OGameNX.boot();
	} 
	catch (/**@type Error*/ ognxe) {
		var /**@type String*/ sErrMsg = "<b>[" + GM_info.script.name + "] " + unsafeWindow.LocalizationStrings.error + "</b>";
		sErrMsg += "<br/><br/>" + ognxe.message;
		unsafeWindow.fadeBox(sErrMsg, true);
	}
});

/**
 * Encapsulates all OGAme NX functions
 * @namespace OGameNX
 * @since 1.0.0.0
 */
var OGameNX = {
	/**
	 * Boot up OGameNX Environment
	 */
	boot : function() {
		OGameNX.registerScript();
		OGameNX.injectStyles();
		OGameNX.buildSettingsMenu();
		//Startup features
		for ( var /**@type Object*/ feature in OGameNX.features) {
			if (typeof (OGameNX.features[feature]) === 'object') {
				if (typeof (OGameNX.features[feature]['boot']) === 'function') {
					OGameNX.features[feature].boot();
				}
			}
		}
	},

	/**
	 * Checks whether the script is up to date
	 * @returns {Boolean} <em>true</em> if the script is up to date, <em>false</em> otherwise
	 */
	isUpToDate : function() {
		var /**@type String*/ sRemoteVersion = $.trim(GM_getResourceText('ONX_CUR_SCRIPT').replace(/[\s\S]*\/\/\s*@version\s+((\d\.?){1,4})[\s\S]*/g, '$1'));
		var /**@type String*/ sCurrentVersion = GM_info.script.version;
		var /**@type Boolean*/ bRes = (OGameNX.helperFunctions.versionCompare(sCurrentVersion, sRemoteVersion) !== 1);
		return bRes;
	},

	/**
	 * Registers OGameNX in the Scripts menu of OGame (if available)
	 * @returns {undefined}
	 */
	registerScript : function() {
		if ($("#oGameVersionCheckData").length > 0) {
			var /**@type jQueryObject*/ oScriptEntry = $('<span style="display: none;"></span>');
			oScriptEntry.append($("<span></span>").html(GM_info.script.name));
			oScriptEntry.append($("<span></span>").html(GM_info.script.version));
			oScriptEntry.append($("<span></span>").html(GM_info.script.namespace));
			oScriptEntry.append($("<span></span>").html(OGameNX.isUpToDate().toString()));
			$("#oGameVersionCheckData").append(oScriptEntry);
		}
	},

	/**
	 * Injects OGame NX' CSS Styles
	 */
	injectStyles : function() {
		$("head").prepend(
			'<style type="text/css">' + 
				'#ogamenx-settings {-webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; padding: 10px; padding-bottom:0; width:100%;} ' + 
				'#ogamenx-settings table {width:100%;} ' + 
				'#ogamenx-settings h3 {padding: 5px; background-color:#13181D; border: 1px solid black; margin-top:10px;text-transform:uppercase;font-size:11px;color:#6f9fc8;text-align:center;font-weight:bold;} ' + 
				'#ogamenx-settings select {visibility:visible;} ' + 
				'#ogamenx-settings .btn_blue {float:left; margin-right:10px;} ' + 
				'#ogamenx-settings strong {font-weight:bold;} ' + 
				'#ogamenx-settings small {font-size:8px;} ' +
				'.ogamenx-diameter, .ogamenx-temperature {font-size:10px!important;}' +
			'</style>'
		);
	},

	buildSettingsMenu : function() {
		var /**@tape jQueryObject*/
		oButton = $('<li id="ogamenx-btn"><span class="menu_icon"><img src="' + this.images.MENU_ICON + '" alt=""/></span><a class="menubutton" href="javascript:void(0)"><span class="textlabel">' + GM_info.script.name + '</span></a></li>');
		if ($("#menuTableTools").length > 0) {
			$("#menuTableTools").first().append(oButton);
		} else if ($("#menuTable").length > 0) {
			$("#menuTable").first().append(oButton);
		} else {
			$(".leftmenu").first().append(oButton);
		}
		oButton.click(function(e) {
			if (!$("#ogamenx-settings").is(":visible")) {
				$("#ogamenx-settings").show();
				$("#inhalt").hide();
			}
		});
		var /**@type jQueryObject*/
		oSettingsForm = $('<form id="ogamenx-settings" style="display: none;">');
		oSettingsForm.append('<table/>');
		var /**@type jQueryObject*/
		oSettingsTable = oSettingsForm.find("table");

		//Logo
		oSettingsTable.append('<tr><td style="text-align:center" colspan="2"><img src="' + this.images.OGAMENX_LOGO + '" alt=""/>');

		//Script
		oSettingsTable.append('<tr><td colspan="2"><h3>' + this.dictionary.getPhrase("SCRIPT_TITLE") + '</h3></td>');
		oSettingsTable.append('<tr>' + '<td><label for="#ogamenx-script-updatenotification">' + this.dictionary.getPhrase("SCRIPT_OPTION_ENABLEUDPATENOTIFICATION") + ':</label></td>' + '<td><input type="checkbox" id="ogamenx-script-updatenotification"/></td>' + '</tr>');

		//Construction
		oSettingsTable.append('<tr><td colspan="2"><h3>' + this.dictionary.getPhrase("CONSTRUCTION_TITLE") + '</h3></td>');
		oSettingsTable.append('<tr>' + '<td><label for="#ogamenx-construction-showpossiblebegin">' + this.dictionary.getPhrase("CONSTRUCTION_OPTION_SHOWPOSSIBLEBEGIN") + ':</label></td>' + '<td><input type="checkbox" id="ogamenx-construction-showpossiblebegin"/></td>' + '</tr>');
		
		//Research
		oSettingsTable.append('<tr><td colspan="2"><h3>' + this.dictionary.getPhrase("RESEARCH_TITLE") + '</h3></td>');
		oSettingsTable.append('<tr>' + '<td><label for="#ogamenx-research-showpossiblebegin">' + this.dictionary.getPhrase("RESEARCH_OPTION_SHOWPOSSIBLEBEGIN") + ':</label></td>' + '<td><input type="checkbox" id="ogamenx-research-showpossiblebegin"/></td>' + '</tr>');

		//Resource Bar
		oSettingsTable.append('<tr><td colspan="2"><h3>' + this.dictionary.getPhrase("RESOURCEBAR_TITLE") + '</h3></td>');
		oSettingsTable.append('<tr>' + '<td><label for="#ogamenx-resourcebar-capacity">' + this.dictionary.getPhrase("RESOURCEBAR_OPTION_SHOWSTORAGECAPACITY") + ':</label></td>' + '<td><input type="checkbox" id="ogamenx-resourcebar-capacity"/></td>' + '</tr>');
		oSettingsTable.append('<tr>' + '<td><label for="#ogamenx-resourcebar-remainingtime">' + this.dictionary.getPhrase("RESOURCEBAR_OPTION_SHOWREMAININGTIME") + ':</label></td>' + '<td><input type="checkbox" id="ogamenx-resourcebar-remainingtime"/></td>' + '</tr>');

		//Planet List
		oSettingsTable.append('<tr><td colspan="2"><h3>' + this.dictionary.getPhrase("PLANETLIST_TITLE") + '</h3></td>');
		oSettingsTable.append('<tr>' + '<td><label for="#ogamenx-planetlist-showsize">' + this.dictionary.getPhrase("PLANETLIST_OPTION_SHOWSIZE") + ':</label></td>' + '<td><input type="checkbox" id="ogamenx-planetlist-showsize"/></td>' + '</tr>');
		oSettingsTable.append('<tr>' + '<td><label for="#ogamenx-planetlist-showtemperature">' + this.dictionary.getPhrase("PLANETLIST_OPTION_SHOWTEMPERATURE") + ':</label></td>' + '<td><input type="checkbox" id="ogamenx-planetlist-showtemperature"/></td>' + '</tr>');
		oSettingsTable.append('<tr>' + '<td><label for="#ogamenx-planetlist-showactivemoondata">' + this.dictionary.getPhrase("PLANETLIST_OPTION_SHOWACTIVEMOONDATA") + ':</label></td>' + '<td><input type="checkbox" id="ogamenx-planetlist-showactivemoondata"/></td>' + '</tr>');
		oSettingsTable.append('<tr style="display:none">' + '<td><label for="#ogamenx-planetlist-remainingtime">' + this.dictionary.getPhrase("PLANETLIST_OPTION_SHOWREMAININGTIME") + ':</label></td>' + '<td>' + '<input type="checkbox" id="ogamenx-planetlist-remainingtime"/>' + '<strong>(' + this.dictionary.getPhrase("EXPERIMENTAL") + ')</strong>' + '</td>' + '</tr>');
		oSettingsTable.append('<tr>' + '<td><label for="#ogamenx-planetlist-order">' + this.dictionary.getPhrase("PLANETLIST_OPTION_ORDERBY") + ':</label></td>' + '<td>' + '<select id="ogamenx-planetlist-order">' + '<select>' + '</td>' + '</tr>');
		var /**@type Array<OGameNX.helperClasses.SortOrder>*/
		aSortOrders = this.helperFunctions.getSorderOrders();
		for ( var i = 0; i < aSortOrders.length; i++) {
			var curSortOrder = aSortOrders[i];
			oSettingsForm.find("#ogamenx-planetlist-order").append('<option value="' + i + '">' + curSortOrder.getLabel() + '</option>');
		}

		//Messages
		oSettingsTable.append('<tr><td colspan="2"><h3>' + this.dictionary.getPhrase("MESSAGES_TITLE") + '</h3></td>');
		oSettingsTable.append('<tr>' + '<td><label for="#ogamenx-messages-showcontent">' + this.dictionary.getPhrase("MESSAGES_OPTION_SHOWCONTENT") + ':</label></td>' + '<td><input type="checkbox" id="ogamenx-messages-showcontent"/></td>' + '</tr>');

		//Save, Reset, Cancel
		oSettingsTable.append('<tr><td><colspan="2"><br/>' + '<a class="btn_blue" href="javascript:void(0)" id="ogamenx-settings-save">' + this.dictionary.getPhrase("SAVE") + '</a>' + '<a class="btn_blue" href="javascript:void(0)" id="ogamenx-settings-reset">' + this.dictionary.getPhrase("RESET") + '</a>' + '<a class="btn_blue" href="javascript:void(0)" id="ogamenx-settings-close">' + this.dictionary.getPhrase("CLOSE") + '</a>' + '</td></tr>');
		$("#contentWrapper").prepend(oSettingsForm);

		/* Initialize Form from stored Settings */
		var /**@type Function*/
		fReadSettings = function() {
			var /**@type Number*/
			nSortOrder = parseInt(GM_getValue("ogamenx-planetlist-order", "0"), 10);
			if (nSortOrder < 0 || nSortOrder >= aSortOrders.length) {
				nSortOrder = 0;
				GM_setValue("ogamenx-planetlist-order", nSortOrder.toString());
			}
			$("#ogamenx-script-updatenotification").attr("checked", (GM_getValue("ogamenx-script-updatenotification", "1") === '1') ? true : false);
			$("#ogamenx-construction-showpossiblebegin").attr("checked", (GM_getValue("ogamenx-construction-showpossiblebegin", "1") === '1') ? true : false);
			$("#ogamenx-research-showpossiblebegin").attr("checked", (GM_getValue("ogamenx-research-showpossiblebegin", "1") === '1') ? true : false);
			$("#ogamenx-resourcebar-capacity").attr("checked", (GM_getValue("ogamenx-resourcebar-capacity", "1") === '1') ? true : false);
			$("#ogamenx-resourcebar-remainingtime").attr("checked", (GM_getValue("ogamenx-resourcebar-remainingtime", "1") === '1') ? true : false);
			$("#ogamenx-messages-showcontent").attr("checked", (GM_getValue("ogamenx-messages-showcontent", "1") === '1') ? true : false);
			$("#ogamenx-planetlist-showsize").attr("checked", (GM_getValue("ogamenx-planetlist-showsize", "1") === '1') ? true : false);
			$("#ogamenx-planetlist-showtemperature").attr("checked", (GM_getValue("ogamenx-planetlist-showtemperature", "1") === '1') ? true : false);
			$("#ogamenx-planetlist-showactivemoondata").attr("checked", (GM_getValue("ogamenx-planetlist-showactivemoondata", "1") === '1') ? true : false);
			
			$("#ogamenx-planetlist-remainingtime").attr("checked", (GM_getValue("ogamenx-planetlist-remainingtime", "0") === '1') ? true : false);
			$('#ogamenx-planetlist-order').find('option[value="' + nSortOrder + '"]').attr("selected", true);
		};
		fReadSettings();

		/* Register Click Handlers */
		$("#ogamenx-settings-reset").unbind("click").click(function(e) {
			GM_setValue("ogamenx-script-updatenotification", "1");
			GM_setValue("ogamenx-construction-showpossiblebegin", "1");
			GM_setValue("ogamenx-research-showpossiblebegin", "1");
			GM_setValue("ogamenx-resourcebar-capacity","1");
			GM_setValue("ogamenx-resourcebar-remainingtime", "1");
			GM_setValue("ogamenx-messages-showcontent", "1");
			GM_setValue("ogamenx-planetlist-showsize", "1");
			GM_setValue("ogamenx-planetlist-showtemperature", "1");
			GM_setValue("ogamenx-planetlist-showactivemoondata", "1");
			GM_setValue("ogamenx-planetlist-remainingtime", "0");
			GM_setValue("ogamenx-planetlist-order", "0");
			fReadSettings();
		});
		$("#ogamenx-settings-save").unbind("click").click(function(e) {
			GM_setValue("ogamenx-script-updatenotification", $("#ogamenx-script-updatenotification").is(":checked") ? "1" : "0");
			GM_setValue("ogamenx-construction-showpossiblebegin", $("#ogamenx-construction-showpossiblebegin").is(":checked") ? "1" : "0");
			GM_setValue("ogamenx-research-showpossiblebegin", $("#ogamenx-research-showpossiblebegin").is(":checked") ? "1" : "0");
			GM_setValue("ogamenx-resourcebar-capacity", $("#ogamenx-resourcebar-capacity").is(":checked") ? "1" : "0");
			GM_setValue("ogamenx-resourcebar-remainingtime", $("#ogamenx-resourcebar-remainingtime").is(":checked") ? "1" : "0");
			GM_setValue("ogamenx-messages-showcontent", $("#ogamenx-messages-showcontent").is(":checked") ? "1" : "0");
			GM_setValue("ogamenx-planetlist-showsize", $("#ogamenx-planetlist-showsize").is(":checked") ? "1" : "0");
			GM_setValue("ogamenx-planetlist-showtemperature", $("#ogamenx-planetlist-showtemperature").is(":checked") ? "1" : "0");
			GM_setValue("ogamenx-planetlist-showtemperature", $("#ogamenx-planetlist-showactivemoondata").is(":checked") ? "1" : "0");
			GM_setValue("ogamenx-planetlist-remainingtime", $("#ogamenx-planetlist-remainingtime").is(":checked") ? "1" : "0");
			GM_setValue("ogamenx-planetlist-order", $('#ogamenx-planetlist-order').find('option:selected').val());
			location.reload();
		});
		$("#ogamenx-settings-close").unbind("click").click(function(e) {
			$("#ogamenx-settings").hide();
			$("#inhalt").show();
		});
	},

	/**
	 * Script Features, every feature (sub object) needs to have a "boot" function which does the neccassary modifications
	 * @namespace
	 */
	features : {
		/**
		 * PlanetList related Feature Functions
		 * @namespace
		 */
		planetlist : {
			boot : function() {
				//Get Required Information
				OGameNX.features.planetlist.gatherInformation();
				OGameNX.features.planetlist.sortPlanetList();
				OGameNX.features.planetlist.showPlanetSize();
				OGameNX.features.planetlist.showPlanetTemperature();
				unsafeWindow.document.cookie="cp=" + $('meta[name="ogame-planet-id"]').attr("content") + ";path=/game";
			},
			gatherInformation: function() {
				var /**@type String*/ sActivePlanetID = $('meta[name="ogame-planet-id"]').attr("content");
				$('#planetList a.planetlink, #planetList a.moonlink').each(function() {
					var /**@type jQueryObject*/ oPlanetLink = $(this);
					var /**@type String*/ sPlanetURL = oPlanetLink.attr("href");
					var /**@type String*/ sPlanetID = OGameNX.helperFunctions.getURLParam("cp", sPlanetURL);
					var /**@type Number*/ nSize = parseInt(GM_getValue("ogamenx-diameter:" + sPlanetID, "NaN"),10);
					var /**@type Number*/ nMinTemp = parseInt(GM_getValue("ogamenx-min-temperature:" + sPlanetID, "NaN"),10);
					var /**@type Number*/ nMaxTemp = parseInt(GM_getValue("ogamenx-max-temperature:" + sPlanetID, "NaN"),10);
					var /**@type Number*/ nUsedFields = parseInt(GM_getValue("ogamenx-used-fields:" + sPlanetID, "NaN"), 10);
					var /**@type Number*/ nTotalFields = NaN;
					if (isNaN(nSize) || isNaN(nMinTemp) || isNaN(nMaxTemp) || isNaN(nUsedFields) || isNaN(nTotalFields)) {
						//Numbers not cached, try extraction from title attribute
						var /**@type String*/ sTitleAttr = oPlanetLink.attr("title").replace(unsafeWindow.LocalizationStrings.thousandSeperator, "").replace(unsafeWindow.LocalizationStrings.decimalPoint, ".").replace(/\s/g, "");
						nSize = parseInt(sTitleAttr.replace(/^.*<br>(\d+)km.*$/gi, "$1"), 10);
						nMinTemp = parseInt(sTitleAttr.replace(/^.*<br>.*<br>(-?\d+).*$/gi, "$1"), 10);
						nMaxTemp = parseInt(sTitleAttr.replace(/^.*<br>.*<br>.*\�[^\d-]*(-?\d+)\�.*$/gi, "$1"), 10);
						nUsedFields = parseInt(sTitleAttr.replace(/^.*<br>\d+km.*\((\d+)\/(\d+)\).*$/gi,"$1"),10);
						nTotalFields = parseInt(sTitleAttr.replace(/^.*<br>\d+km.*\((\d+)\/(\d+)\).*$/gi,"$2"),10);
						if (isNaN(nSize) || isNaN(nMinTemp) || isNaN(nMaxTemp) || isNaN(nUsedFields) || isNaN(nTotalFields)) {	
							if (sPlanetID === sActivePlanetID) {
								//We'll extract the needed information from the current source code#
								//This is only needed if the planet was hovered during this phase
								if (isNaN(nSize)) {
									nSize = parseInt(
											$("html").html().replace(/[\s\S]*textContent\[1\]="([^"]+)"[\s\S]*/g, "$1")
												.replace(/\\\//g, "/")
												.replace(unsafeWindow.LocalizationStrings.thousandSeperator, "")
												.replace(unsafeWindow.LocalizationStrings.decimalPoint, ".")
												.replace(/\s/g, "").replace(/^.*?(\d+)km.*$/gi, "$1")
											, 10);
								}
								if (isNaN(nMinTemp)) {
									nMinTemp = parseInt(
												$("html").html()
													.replace(/[\s\S]*textContent\[3\]="([^"]+)"[\s\S]*/g, "$1")
													.replace(/\\u[a-zA-Z0-9]+/g, "")
													.replace(/\\\//g, "/")
													.replace(unsafeWindow.LocalizationStrings.thousandSeperator, "")
													.replace(unsafeWindow.LocalizationStrings.decimalPoint, ".")
													.replace(/\s/g, "")
													.replace(/^.*?(-?\d+).*?(-?\d+).*$/gi, "$1")
												, 10);
								}
								if (isNaN(nMaxTemp)) {
									nMaxTemp = parseInt(
												$("html").html()
													.replace(/[\s\S]*textContent\[3\]="([^"]+)"[\s\S]*/g, "$1")
													.replace(/\\u[a-zA-Z0-9]+/g, "")
													.replace(/\\\//g, "/")
													.replace(unsafeWindow.LocalizationStrings.thousandSeperator, "")
													.replace(unsafeWindow.LocalizationStrings.decimalPoint, ".")
													.replace(/\s/g, "")
													.replace(/^.*?(-?\d+).*?(-?\d+).*$/gi, "$2")
												, 10);
								}
								if (isNaN(nUsedFields)) {
									nUsedFields = parseInt(
													$("html").html().replace(/[\s\S]*textContent\[1\]="([^"]+)"[\s\S]*/g, "$1")
													.replace(/\\\//g, "/")
													.replace(/\s/g, "").replace(/^<\/?span>/gi, "")
													.replace(/.*km.*\((\d+)\/(\d+)\).*$/gi,"$1")
												 ,10);
								}
								if (isNaN(nTotalFields)) {
									nTotalFields = parseInt(
													$("html").html().replace(/[\s\S]*textContent\[1\]="([^"]+)"[\s\S]*/g, "$1")
													.replace(/\\\//g, "/")
													.replace(/\s/g, "").replace(/^<\/?span>/gi, "")
													.replace(/.*km.*\((\d+)\/(\d+)\).*$/gi,"$2")
												 ,10);
								}
							} 
							else {
								//Okay we'll need to request the details for the planet
								//But this will be done synchronously... This is fast enough, easier to handle and 
								//is only done during this phase if a planet was hovered which is not the currently active one
								$.ajax({
									url : sPlanetURL,
									async : false,
									data : null,
									success : function(data) {
										if (isNaN(nSize)) {
											nSize = parseInt(
														data
															.replace(/[\s\S]*textContent\[1\]="([^"]+)"[\s\S]*/g, "$1")
															.replace(/\\\//g, "/")
															.replace(unsafeWindow.LocalizationStrings.thousandSeperator, "")
															.replace(unsafeWindow.LocalizationStrings.decimalPoint, ".")
															.replace(/\s/g, "")
															.replace(/^.*?(\d+)km.*$/gi, "$1")
														, 10);
										}
										if (isNaN(nMinTemp)) {
											nMinTemp = parseInt(	
														data
															.replace(/[\s\S]*textContent\[3\]="([^"]+)"[\s\S]*/g, "$1")
															.replace(/\\u[a-zA-Z0-9]+/g, "")
															.replace(/\\\//g, "/")
															.replace(unsafeWindow.LocalizationStrings.thousandSeperator, "")
															.replace(unsafeWindow.LocalizationStrings.decimalPoint, ".")
															.replace(/\s/g, "")
															.replace(/^.*?(-?\d+).*?(-?\d+).*$/gi, "$1")
														, 10);
										}
										if (isNaN(nMaxTemp)) {
											nMaxTemp = parseInt(
														data
															.replace(/[\s\S]*textContent\[3\]="([^"]+)"[\s\S]*/g, "$1")
															.replace(/\\u[a-zA-Z0-9]+/g, "")
															.replace(/\\\//g, "/")
															.replace(unsafeWindow.LocalizationStrings.thousandSeperator, "")
															.replace(unsafeWindow.LocalizationStrings.decimalPoint, ".")
															.replace(/\s/g, "")
															.replace(/^.*?(-?\d+).*?(-?\d+).*$/gi, "$2")
														, 10);
										}
										if (isNaN(nUsedFields)) {
											nUsedFields = parseInt(
														data
															.replace(/[\s\S]*textContent\[1\]="([^"]+)"[\s\S]*/g, "$1")
															.replace(/\\\//g, "/")
															.replace(/\s/g, "").replace(/^<\/?span>/gi, "")
															.replace(/.*km.*\((\d+)\/(\d+)\).*$/gi,"$1")
														 ,10);
										}
										if (isNaN(nTotalFields)) {
											nTotalFields = parseInt(
														data
															.replace(/[\s\S]*textContent\[1\]="([^"]+)"[\s\S]*/g, "$1")
															.replace(/\\\//g, "/")
															.replace(/\s/g, "").replace(/^<\/?span>/gi, "")
															.replace(/.*km.*\((\d+)\/(\d+)\).*$/gi,"$2")
														 ,10);
										}
									}
								});
							}
						}
						//Write found values into cache
						GM_setValue("ogamenx-diameter:" + sPlanetID, nSize.toString());
						GM_setValue("ogamenx-min-temperature:" + sPlanetID, nMinTemp.toString());
						GM_setValue("ogamenx-max-temperature:" + sPlanetID, nMaxTemp.toString());
						GM_setValue("ogamenx-used-fields:" + sPlanetID, nUsedFields.toString());
						GM_setValue("ogamenx-total-fields:" + sPlanetID, nTotalFields.toString());
					}
					//Write found information of planet into attributes
					oPlanetLink.attr("ogamenx-diameter",nSize.toString());
					oPlanetLink.attr("ogamenx-min-temperature",nMinTemp.toString());
					oPlanetLink.attr("ogamenx-max-temperature",nMaxTemp.toString());
					oPlanetLink.attr("ogamenx-used-fields",nUsedFields.toString());
					oPlanetLink.attr("ogamenx-total-fields",nTotalFields.toString());
					
					//Show Moon's data
					if ($(this).hasClass("planetlink") && $(this).next(".moonlink.active")[0] && GM_getValue("ogamenx-planetlist-showactivemoondata", "1")==="1") {
						sPlanetID = OGameNX.helperFunctions.getURLParam("cp",$(this).next(".moonlink.active").attr("href"));
						$(this).children(".planet-name").first().text($('meta[name="ogame-planet-name"]').attr("content"));
					}
				});
			},
			sortPlanetList : function() {
				var /** @type Array<OGameNX.helperClasses.SortOrder> */
				aSortOrders = OGameNX.helperFunctions.getSorderOrders();
				var /** @type Number */
				nSortOrder = parseInt(GM_getValue("ogamenx-planetlist-order", "0"), 10);
				if (nSortOrder < 0 || nSortOrder >= aSortOrders.length) {
					nSortOrder = 0;
					GM_setValue("ogamenx-planetlist-order", nSortOrder.toString());
				}
				var /**@type OGameNX.helperClasses.SortOrder */
				oSortOrder = aSortOrders[nSortOrder];
				if (typeof (oSortOrder.getComparatorFunction()) === 'function') {
					$('#planetList div[class*="planet"]').sortElements(oSortOrder.getComparatorFunction());
				}
			},
			showPlanetSize: function() {
				if (GM_getValue("ogamenx-planetlist-showsize","1")==="1") {
					$("#planetList .planetlink").each(function() {
						var /**@type String*/ sPlanetID = OGameNX.helperFunctions.getURLParam("cp", $(this).attr("href"));
						//Show Moon's data
						if ($(this).next(".moonlink.active")[0] && GM_getValue("ogamenx-planetlist-showactivemoondata", "1")==="1") {
							sPlanetID = OGameNX.helperFunctions.getURLParam("cp",$(this).next(".moonlink.active").attr("href"));
							$(this).children(".planet-name").first().text($('meta[name="ogame-planet-name"]').attr("content"));
						}
						$(this).append('<br/><span class="planet-koords ogamenx-diameter ogamenx-diameter-' + sPlanetID + '">' + OGameNX.helperFunctions.formatNumber(parseInt(GM_getValue("ogamenx-diameter:"+sPlanetID))) + ' km (' + GM_getValue("ogamenx-used-fields:"+sPlanetID) + '/' + GM_getValue("ogamenx-total-fields:"+sPlanetID) + ')</span>');
						$(this).parent().height($(this).height());
						$(this).next(".moonlink").height($(this).height());
					});
				}
			},
			showPlanetTemperature: function() {
				if (GM_getValue("ogamenx-planetlist-showtemperature","1")==="1") {
					$("#planetList .planetlink").each(function() {
						var /**@type String*/ sPlanetID = OGameNX.helperFunctions.getURLParam("cp", $(this).attr("href"));
						//Show Moon's data
						if ($(this).next(".moonlink.active")[0] && GM_getValue("ogamenx-planetlist-showactivemoondata", "1")==="1") {
							sPlanetID = OGameNX.helperFunctions.getURLParam("cp",$(this).next(".moonlink.active").attr("href"));
							$(this).children(".planet-name").first().text(($('meta[name="ogame-planet-name"]').attr("content")));
						}
						$(this).append('<br/><span class="planet-koords ogamenx-temperature ogamenx-temperature-' + sPlanetID + '">' + OGameNX.helperFunctions.formatNumber(parseInt(GM_getValue("ogamenx-min-temperature:"+sPlanetID))) + '�C - ' + OGameNX.helperFunctions.formatNumber(parseInt(GM_getValue("ogamenx-max-temperature:"+sPlanetID))) + '�C</span>');
						$(this).parent().height($(this).height());
						$(this).next(".moonlink").height($(this).height());
					});
				}
			}
		},

		/**
		 * Messages related Feature Functions
		 * @namespace
		 */
		messages : {
			boot : function() {

			},
		},
		/**
		 * Constrcution related Feature Functions
		 * @namespace
		 */
		construction : {
			boot : function() {

			},
		},

		/**
		 * Research related Feature Functions
		 * @namespace
		 */
		research : {
			boot : function() {

			},
		},

		/**
		 * Resourcebar related Feature Functions
		 * @namespace
		 */
		resourcebar : {
			boot : function() {
			},
		},
	},

	/**
	 * Base64 Coded Images
	 * @namespace
	 */
	images : {
		MENU_ICON : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAAdCAYAAADGgB7AAAAACXBIWXMAAA7EAAAOxAGVKw4bAAA8jmlUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4KPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS41LWMwMTQgNzkuMTUxNDgxLCAyMDEzLzAzLzEzLTEyOjA5OjE1ICAgICAgICAiPgogICA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPgogICAgICA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIgogICAgICAgICAgICB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iCiAgICAgICAgICAgIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyIKICAgICAgICAgICAgeG1sbnM6cGhvdG9zaG9wPSJodHRwOi8vbnMuYWRvYmUuY29tL3Bob3Rvc2hvcC8xLjAvIgogICAgICAgICAgICB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIKICAgICAgICAgICAgeG1sbnM6c3RFdnQ9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZUV2ZW50IyIKICAgICAgICAgICAgeG1sbnM6dGlmZj0iaHR0cDovL25zLmFkb2JlLmNvbS90aWZmLzEuMC8iCiAgICAgICAgICAgIHhtbG5zOmV4aWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20vZXhpZi8xLjAvIj4KICAgICAgICAgPHhtcDpDcmVhdG9yVG9vbD5BZG9iZSBQaG90b3Nob3AgQ0MgKFdpbmRvd3MpPC94bXA6Q3JlYXRvclRvb2w+CiAgICAgICAgIDx4bXA6Q3JlYXRlRGF0ZT4yMDEzLTA4LTEwVDEyOjQ1OjE0KzAyOjAwPC94bXA6Q3JlYXRlRGF0ZT4KICAgICAgICAgPHhtcDpNb2RpZnlEYXRlPjIwMTMtMDgtMTBUMTM6MTk6MDcrMDI6MDA8L3htcDpNb2RpZnlEYXRlPgogICAgICAgICA8eG1wOk1ldGFkYXRhRGF0ZT4yMDEzLTA4LTEwVDEzOjE5OjA3KzAyOjAwPC94bXA6TWV0YWRhdGFEYXRlPgogICAgICAgICA8ZGM6Zm9ybWF0PmltYWdlL3BuZzwvZGM6Zm9ybWF0PgogICAgICAgICA8cGhvdG9zaG9wOkNvbG9yTW9kZT4zPC9waG90b3Nob3A6Q29sb3JNb2RlPgogICAgICAgICA8cGhvdG9zaG9wOkRvY3VtZW50QW5jZXN0b3JzPgogICAgICAgICAgICA8cmRmOkJhZz4KICAgICAgICAgICAgICAgPHJkZjpsaT5BODJDMEU2NjYyNzU0RkY5NzU3MDU1MUE0RDA2MUYyRDwvcmRmOmxpPgogICAgICAgICAgICAgICA8cmRmOmxpPnhtcC5kaWQ6MTFmODFkYjItNzkxYi0zNjQ5LTljYTItOWE4OTMwMmE4MWRlPC9yZGY6bGk+CiAgICAgICAgICAgIDwvcmRmOkJhZz4KICAgICAgICAgPC9waG90b3Nob3A6RG9jdW1lbnRBbmNlc3RvcnM+CiAgICAgICAgIDx4bXBNTTpJbnN0YW5jZUlEPnhtcC5paWQ6YTg2ZWE3MjUtYTBlMy00MTQ4LWE1ZDAtMWVmZjJjM2U3YTAyPC94bXBNTTpJbnN0YW5jZUlEPgogICAgICAgICA8eG1wTU06RG9jdW1lbnRJRD54bXAuZGlkOjE1MDExZWY4LWYxMDEtZTc0YS1hOTQ3LTA5MjRkMDkxMjE3NzwveG1wTU06RG9jdW1lbnRJRD4KICAgICAgICAgPHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD54bXAuZGlkOjE1MDExZWY4LWYxMDEtZTc0YS1hOTQ3LTA5MjRkMDkxMjE3NzwveG1wTU06T3JpZ2luYWxEb2N1bWVudElEPgogICAgICAgICA8eG1wTU06SGlzdG9yeT4KICAgICAgICAgICAgPHJkZjpTZXE+CiAgICAgICAgICAgICAgIDxyZGY6bGkgcmRmOnBhcnNlVHlwZT0iUmVzb3VyY2UiPgogICAgICAgICAgICAgICAgICA8c3RFdnQ6YWN0aW9uPmNyZWF0ZWQ8L3N0RXZ0OmFjdGlvbj4KICAgICAgICAgICAgICAgICAgPHN0RXZ0Omluc3RhbmNlSUQ+eG1wLmlpZDoxNTAxMWVmOC1mMTAxLWU3NGEtYTk0Ny0wOTI0ZDA5MTIxNzc8L3N0RXZ0Omluc3RhbmNlSUQ+CiAgICAgICAgICAgICAgICAgIDxzdEV2dDp3aGVuPjIwMTMtMDgtMTBUMTI6NDU6MTQrMDI6MDA8L3N0RXZ0OndoZW4+CiAgICAgICAgICAgICAgICAgIDxzdEV2dDpzb2Z0d2FyZUFnZW50PkFkb2JlIFBob3Rvc2hvcCBDQyAoV2luZG93cyk8L3N0RXZ0OnNvZnR3YXJlQWdlbnQ+CiAgICAgICAgICAgICAgIDwvcmRmOmxpPgogICAgICAgICAgICAgICA8cmRmOmxpIHJkZjpwYXJzZVR5cGU9IlJlc291cmNlIj4KICAgICAgICAgICAgICAgICAgPHN0RXZ0OmFjdGlvbj5zYXZlZDwvc3RFdnQ6YWN0aW9uPgogICAgICAgICAgICAgICAgICA8c3RFdnQ6aW5zdGFuY2VJRD54bXAuaWlkOmNlZjBjMTVlLTYyMzAtOTM0Ni1hMWQ0LWM3MTYyMjY1ZTAyZTwvc3RFdnQ6aW5zdGFuY2VJRD4KICAgICAgICAgICAgICAgICAgPHN0RXZ0OndoZW4+MjAxMy0wOC0xMFQxMjo0OTo0MiswMjowMDwvc3RFdnQ6d2hlbj4KICAgICAgICAgICAgICAgICAgPHN0RXZ0OnNvZnR3YXJlQWdlbnQ+QWRvYmUgUGhvdG9zaG9wIENDIChXaW5kb3dzKTwvc3RFdnQ6c29mdHdhcmVBZ2VudD4KICAgICAgICAgICAgICAgICAgPHN0RXZ0OmNoYW5nZWQ+Lzwvc3RFdnQ6Y2hhbmdlZD4KICAgICAgICAgICAgICAgPC9yZGY6bGk+CiAgICAgICAgICAgICAgIDxyZGY6bGkgcmRmOnBhcnNlVHlwZT0iUmVzb3VyY2UiPgogICAgICAgICAgICAgICAgICA8c3RFdnQ6YWN0aW9uPnNhdmVkPC9zdEV2dDphY3Rpb24+CiAgICAgICAgICAgICAgICAgIDxzdEV2dDppbnN0YW5jZUlEPnhtcC5paWQ6YTg2ZWE3MjUtYTBlMy00MTQ4LWE1ZDAtMWVmZjJjM2U3YTAyPC9zdEV2dDppbnN0YW5jZUlEPgogICAgICAgICAgICAgICAgICA8c3RFdnQ6d2hlbj4yMDEzLTA4LTEwVDEzOjE5OjA3KzAyOjAwPC9zdEV2dDp3aGVuPgogICAgICAgICAgICAgICAgICA8c3RFdnQ6c29mdHdhcmVBZ2VudD5BZG9iZSBQaG90b3Nob3AgQ0MgKFdpbmRvd3MpPC9zdEV2dDpzb2Z0d2FyZUFnZW50PgogICAgICAgICAgICAgICAgICA8c3RFdnQ6Y2hhbmdlZD4vPC9zdEV2dDpjaGFuZ2VkPgogICAgICAgICAgICAgICA8L3JkZjpsaT4KICAgICAgICAgICAgPC9yZGY6U2VxPgogICAgICAgICA8L3htcE1NOkhpc3Rvcnk+CiAgICAgICAgIDx0aWZmOk9yaWVudGF0aW9uPjE8L3RpZmY6T3JpZW50YXRpb24+CiAgICAgICAgIDx0aWZmOlhSZXNvbHV0aW9uPjk2MDAwMC8xMDAwMDwvdGlmZjpYUmVzb2x1dGlvbj4KICAgICAgICAgPHRpZmY6WVJlc29sdXRpb24+OTYwMDAwLzEwMDAwPC90aWZmOllSZXNvbHV0aW9uPgogICAgICAgICA8dGlmZjpSZXNvbHV0aW9uVW5pdD4yPC90aWZmOlJlc29sdXRpb25Vbml0PgogICAgICAgICA8ZXhpZjpDb2xvclNwYWNlPjY1NTM1PC9leGlmOkNvbG9yU3BhY2U+CiAgICAgICAgIDxleGlmOlBpeGVsWERpbWVuc2lvbj4zODwvZXhpZjpQaXhlbFhEaW1lbnNpb24+CiAgICAgICAgIDxleGlmOlBpeGVsWURpbWVuc2lvbj4yOTwvZXhpZjpQaXhlbFlEaW1lbnNpb24+CiAgICAgIDwvcmRmOkRlc2NyaXB0aW9uPgogICA8L3JkZjpSREY+CjwveDp4bXBtZXRhPgogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgIAo8P3hwYWNrZXQgZW5kPSJ3Ij8+MeNbEAAAACBjSFJNAAB6JQAAgIMAAPn/AACA6QAAdTAAAOpgAAA6mAAAF2+SX8VGAAARZUlEQVR4AQBVEaruAf///wAAAAAAAAAAACAmK8H9/Ps+AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADBAXC4NrVPwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAAAAAAAAICUqwf38+wIAAAAAAAAAAAAAAAAA/wAAAP8AAAD/AAAA/wAAAP8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP8AAAD/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHCEkxQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAAAgJivB/f38AgAAADwAAAAAAP8AAAD/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD/AAAA/wAAAP8AAAD/AAAA/wAAAAAAAAAAAAAA/wAAAAAAAAAAAAAAAAAAAAAAAAAAAAD9+/s+AAAAABcbH8kAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAAD39fQIAAAAPAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP8AAAD/AAAA/wAAAP8AAAD/AAAAAAAAAgIEOgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAP8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA///+AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAAAAAAAA/v79AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD/AP8AAP8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAgIDAAAAAACFg38AIB8fAMbGxwDq6+sAR0ZFAAAAAAAEBAQA8PDwALu8vQC1t7oAAAAAAG1raAA2NTQA8fDxAHyAggDw8PEAAAAAAAQEBACCf3sAGBgYAOHi4gCBg4cA/wD/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP///wAWGh7JHCAl/xwgJf////////////////////////////////////////////////94eXv/HCAl/zQ3O////////////6mpqv8cICX/HCAl/62vsP///////////1NVWP8bICT/GyAk/xYaHsn///8A////AP///wD///8A////AP///wD///8A////AP///wD///8AAgAAAAAAAAAAAAAAAAAAAADj4+MAAAAAAAAAAAD29vYAycnLAMzNzgDR0dIA/Pz8AAAAAACHhoQADw4NAOjp6gCbnZ8AAAAAAFZWVQAEAwMAAAAAAFJQTwAAAAAAzs/QAMnLzQABAAAAAQEBAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAAAAAAAAAAAAAAAAAAD8/f0AAAAAAN/f3wAyNToAVFhbAFFUWABMUFQAkZKUAAAAAAAAAAAALy8vAAAAAACDhYgA/P39AAAAAABXVlMAQ0A+AAAAAAAAAAAAXmBjAAAAAAAAAAEAAP8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAAAAAAAA///+AAAAAAD//v4AAAAAAN3e3wD19vYAAAAAAAAAAAAAAAAAycrMAAAAAAAAAAAABwcHAAAAAAD///8AZGZoAPf4+ACIhoQAoJ+cAAAAAACMjo8A8fLzAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAA+/v7AAYGBwD9/f4AAAAAABAQEAAAAAAAAAAAAAAAAAAAAAAAAgMCAAAAAAAAAAAA9vb1AAAAAAAAAAAAvb7BAIiLiwBJT0wAEA0OAO/r7QCUlpoAAAAAAAAAAAAAAAAA+/v6AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAAADx7+wQDxEUAPr6+QD9/f0AAAAAAAICAQD6+vkAAAAAAAAAAAAAAAAA6+vqAAAAAAD3+foA2traAPr6+QAAAAAAAAAAAN/f4AAEBAQA7/P0AL2+vwD29vUAAAAAAAAAAAABAQEAAAAAAPHu6xAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP///wAAAADfAAAA/wAAAP/a29v//////7/AwP8AAAD/AAAA/wAAAP8AAAD/Ghoa/7rBwP+dp6X/DQ4O/wAAAP8AAAD/BgcH/8TNyv/h6+f/3ufm/9vj4v8NDg7/AAAA/wAAAP8AAAD/AAAA/wAAAN////8A////AP///wD///8A////AP///wD///8A////AP///wD///8AAgAAAAAAAAAAAAAAAAAAAADZ3dwAztTUALK1tAAAAAAAAAAAAAAAAAAAAAAA9vf3ANja2gD8/PwAAgICAAAAAAAAAAAAcXJ1ADsyNQDn5ucA5+PhACQcHQCXn50AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAADBwcEAGRobAPHx8AAAAAAAAAAAAAAAAAAAAAAABQUFACIgIgACBAQABAQFAAAAAAAfICAAe359AAAAAABya24A2djYAOXo6ABbUlQAjDExANDPzwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAAAAAAAAAAAAAAAAAAAJCAoADhASAB4eHwAAAAAAAAAAAAAAAAAAAAAABAQEABUYFwAXFxkAAQEAAAAAAACgoJ8ADQgGAAAAAADMysoA7ezsAOjo6AAAAAAAwcDAAAEBAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAAAAAAAAAAAAAAAAAAAhJCMAExcVACAfIAAAAAAAAAAAAAAAAAAAAAAABwgHABsaGQAbGRgA9fX1ADY2NgBAP0AAAAAAAIaGhgD6+voAAAAAAHt3dwD///8ADg4OAGRkZAAAAAAAAQICAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAACHgYEACgsLAPn39wAAAAAAAAAAAAAAAAAAAAAA6ObnAFpSUwADAwMA+fj4APPz8wBFRUUA/v7+AMDAwAAAAAAAAAAAAKWlpQDj4+MABAQEAAsLCwAAAAAA//7+AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP///wAAAADfAAAA/wAAAP8AAAD/AAAA/wAAAP8AAAD/AAAA/wAAAP8AAAD/AAAA/wAAAP8AAAD/AAAA/wAAAP8AAAD/AAAA/wAAAP8AAAD/AAAA/wAAAP8AAAD/AAAA/wAAAP8AAAD/AAAA/wAAAN////8A////AP///wD///8A////AP///wD///8A////AP///wD///8AAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAAAGCAn6AAAAAAYHCQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAAAADAwX+CQwE2AD/ACgJBA4AAAH/AP79/gAAAQAAAP7/AAAAAAAAAAAAAAAAAAAAAAAAAAAA+fj2AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABwgKAAAAAAD/AP8AAwIC2AD/AQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAf///wAAAAAADRET0wH/AAD/AP8sAAAAAP//AAAA//8AAAAAAAAAAAD/AP8AAAAAAAAAAAAA/wAAAAAAAAAAAAAAAP8AAAABAAAAAAAAAP8AAAECAAAAAAD28/AACw4RAAEB/wAAAADUAAAAAPPw7y0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAf///wAAAAAAAAAAABMXGs0A/wAA/v//AgAA/wAA//8AAAAAAAAAAAD/AP8CAP8AAAABAAAA/wAAAAAAAAABAAAA/wEAAAD/AAABAAAA/wEAAAD/AAABAAAA/wEAAAD/AAIDA/4AAAAA7urnMQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAf///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAA//+X/ocMPL6KgQAAAABJRU5ErkJggg==",
		OGAMENX_LOGO : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAU4AAABwCAYAAABxVJkAAAAACXBIWXMAAC4jAAAuIwF4pT92AAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAADyJaVRYdFhNTDpjb20uYWRvYmUueG1wAAAAAAA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/Pgo8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjUtYzAxNCA3OS4xNTE0ODEsIDIwMTMvMDMvMTMtMTI6MDk6MTUgICAgICAgICI+CiAgIDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+CiAgICAgIDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiCiAgICAgICAgICAgIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIKICAgICAgICAgICAgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iCiAgICAgICAgICAgIHhtbG5zOnN0RXZ0PSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VFdmVudCMiCiAgICAgICAgICAgIHhtbG5zOnBob3Rvc2hvcD0iaHR0cDovL25zLmFkb2JlLmNvbS9waG90b3Nob3AvMS4wLyIKICAgICAgICAgICAgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIgogICAgICAgICAgICB4bWxuczp0aWZmPSJodHRwOi8vbnMuYWRvYmUuY29tL3RpZmYvMS4wLyIKICAgICAgICAgICAgeG1sbnM6ZXhpZj0iaHR0cDovL25zLmFkb2JlLmNvbS9leGlmLzEuMC8iPgogICAgICAgICA8eG1wOkNyZWF0b3JUb29sPkFkb2JlIFBob3Rvc2hvcCBDQyAoV2luZG93cyk8L3htcDpDcmVhdG9yVG9vbD4KICAgICAgICAgPHhtcDpDcmVhdGVEYXRlPjIwMTMtMDgtMTBUMTM6MTI6MDgrMDI6MDA8L3htcDpDcmVhdGVEYXRlPgogICAgICAgICA8eG1wOk1ldGFkYXRhRGF0ZT4yMDEzLTA4LTEwVDEzOjE5OjQ0KzAyOjAwPC94bXA6TWV0YWRhdGFEYXRlPgogICAgICAgICA8eG1wOk1vZGlmeURhdGU+MjAxMy0wOC0xMFQxMzoxOTo0NCswMjowMDwveG1wOk1vZGlmeURhdGU+CiAgICAgICAgIDx4bXBNTTpJbnN0YW5jZUlEPnhtcC5paWQ6ZjZkODZkYTItYmM3Mi04MTQxLTkxMzAtMjQ2ZTk1ZmU3MWZlPC94bXBNTTpJbnN0YW5jZUlEPgogICAgICAgICA8eG1wTU06RG9jdW1lbnRJRD54bXAuZGlkOjExZjgxZGIyLTc5MWItMzY0OS05Y2EyLTlhODkzMDJhODFkZTwveG1wTU06RG9jdW1lbnRJRD4KICAgICAgICAgPHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD54bXAuZGlkOjExZjgxZGIyLTc5MWItMzY0OS05Y2EyLTlhODkzMDJhODFkZTwveG1wTU06T3JpZ2luYWxEb2N1bWVudElEPgogICAgICAgICA8eG1wTU06SGlzdG9yeT4KICAgICAgICAgICAgPHJkZjpTZXE+CiAgICAgICAgICAgICAgIDxyZGY6bGkgcmRmOnBhcnNlVHlwZT0iUmVzb3VyY2UiPgogICAgICAgICAgICAgICAgICA8c3RFdnQ6YWN0aW9uPmNyZWF0ZWQ8L3N0RXZ0OmFjdGlvbj4KICAgICAgICAgICAgICAgICAgPHN0RXZ0Omluc3RhbmNlSUQ+eG1wLmlpZDoxMWY4MWRiMi03OTFiLTM2NDktOWNhMi05YTg5MzAyYTgxZGU8L3N0RXZ0Omluc3RhbmNlSUQ+CiAgICAgICAgICAgICAgICAgIDxzdEV2dDp3aGVuPjIwMTMtMDgtMTBUMTM6MTI6MDgrMDI6MDA8L3N0RXZ0OndoZW4+CiAgICAgICAgICAgICAgICAgIDxzdEV2dDpzb2Z0d2FyZUFnZW50PkFkb2JlIFBob3Rvc2hvcCBDQyAoV2luZG93cyk8L3N0RXZ0OnNvZnR3YXJlQWdlbnQ+CiAgICAgICAgICAgICAgIDwvcmRmOmxpPgogICAgICAgICAgICAgICA8cmRmOmxpIHJkZjpwYXJzZVR5cGU9IlJlc291cmNlIj4KICAgICAgICAgICAgICAgICAgPHN0RXZ0OmFjdGlvbj5zYXZlZDwvc3RFdnQ6YWN0aW9uPgogICAgICAgICAgICAgICAgICA8c3RFdnQ6aW5zdGFuY2VJRD54bXAuaWlkOjU5Yjc5MTQ5LTQ0YjktNmI0Yy04YmFiLWM2OGU3NWExNGZjZjwvc3RFdnQ6aW5zdGFuY2VJRD4KICAgICAgICAgICAgICAgICAgPHN0RXZ0OndoZW4+MjAxMy0wOC0xMFQxMzoxMjowOCswMjowMDwvc3RFdnQ6d2hlbj4KICAgICAgICAgICAgICAgICAgPHN0RXZ0OnNvZnR3YXJlQWdlbnQ+QWRvYmUgUGhvdG9zaG9wIENDIChXaW5kb3dzKTwvc3RFdnQ6c29mdHdhcmVBZ2VudD4KICAgICAgICAgICAgICAgICAgPHN0RXZ0OmNoYW5nZWQ+Lzwvc3RFdnQ6Y2hhbmdlZD4KICAgICAgICAgICAgICAgPC9yZGY6bGk+CiAgICAgICAgICAgICAgIDxyZGY6bGkgcmRmOnBhcnNlVHlwZT0iUmVzb3VyY2UiPgogICAgICAgICAgICAgICAgICA8c3RFdnQ6YWN0aW9uPnNhdmVkPC9zdEV2dDphY3Rpb24+CiAgICAgICAgICAgICAgICAgIDxzdEV2dDppbnN0YW5jZUlEPnhtcC5paWQ6ZjZkODZkYTItYmM3Mi04MTQxLTkxMzAtMjQ2ZTk1ZmU3MWZlPC9zdEV2dDppbnN0YW5jZUlEPgogICAgICAgICAgICAgICAgICA8c3RFdnQ6d2hlbj4yMDEzLTA4LTEwVDEzOjE5OjQ0KzAyOjAwPC9zdEV2dDp3aGVuPgogICAgICAgICAgICAgICAgICA8c3RFdnQ6c29mdHdhcmVBZ2VudD5BZG9iZSBQaG90b3Nob3AgQ0MgKFdpbmRvd3MpPC9zdEV2dDpzb2Z0d2FyZUFnZW50PgogICAgICAgICAgICAgICAgICA8c3RFdnQ6Y2hhbmdlZD4vPC9zdEV2dDpjaGFuZ2VkPgogICAgICAgICAgICAgICA8L3JkZjpsaT4KICAgICAgICAgICAgPC9yZGY6U2VxPgogICAgICAgICA8L3htcE1NOkhpc3Rvcnk+CiAgICAgICAgIDxwaG90b3Nob3A6Q29sb3JNb2RlPjM8L3Bob3Rvc2hvcDpDb2xvck1vZGU+CiAgICAgICAgIDxwaG90b3Nob3A6SUNDUHJvZmlsZT5zUkdCIElFQzYxOTY2LTIuMTwvcGhvdG9zaG9wOklDQ1Byb2ZpbGU+CiAgICAgICAgIDxwaG90b3Nob3A6RG9jdW1lbnRBbmNlc3RvcnM+CiAgICAgICAgICAgIDxyZGY6QmFnPgogICAgICAgICAgICAgICA8cmRmOmxpPkE4MkMwRTY2NjI3NTRGRjk3NTcwNTUxQTREMDYxRjJEPC9yZGY6bGk+CiAgICAgICAgICAgIDwvcmRmOkJhZz4KICAgICAgICAgPC9waG90b3Nob3A6RG9jdW1lbnRBbmNlc3RvcnM+CiAgICAgICAgIDxkYzpmb3JtYXQ+aW1hZ2UvcG5nPC9kYzpmb3JtYXQ+CiAgICAgICAgIDx0aWZmOk9yaWVudGF0aW9uPjE8L3RpZmY6T3JpZW50YXRpb24+CiAgICAgICAgIDx0aWZmOlhSZXNvbHV0aW9uPjMwMDAwMDAvMTAwMDA8L3RpZmY6WFJlc29sdXRpb24+CiAgICAgICAgIDx0aWZmOllSZXNvbHV0aW9uPjMwMDAwMDAvMTAwMDA8L3RpZmY6WVJlc29sdXRpb24+CiAgICAgICAgIDx0aWZmOlJlc29sdXRpb25Vbml0PjI8L3RpZmY6UmVzb2x1dGlvblVuaXQ+CiAgICAgICAgIDxleGlmOkNvbG9yU3BhY2U+MTwvZXhpZjpDb2xvclNwYWNlPgogICAgICAgICA8ZXhpZjpQaXhlbFhEaW1lbnNpb24+MzM0PC9leGlmOlBpeGVsWERpbWVuc2lvbj4KICAgICAgICAgPGV4aWY6UGl4ZWxZRGltZW5zaW9uPjExMjwvZXhpZjpQaXhlbFlEaW1lbnNpb24+CiAgICAgIDwvcmRmOkRlc2NyaXB0aW9uPgogICA8L3JkZjpSREY+CjwveDp4bXBtZXRhPgogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgIAo8P3hwYWNrZXQgZW5kPSJ3Ij8+4jjsPQAAACBjSFJNAAB6JQAAgIMAAPn/AACA6QAAdTAAAOpgAAA6mAAAF2+SX8VGAACQxUlEQVR42ux9Z3hc1dX1OufcOr1o1G1LtuVesammB0xvoYaEUJKQShLypfdA+hsS3jTSSCG0ACGE3oONsTEuuBsXWVax+vR2+/l+3JmxJEtGBELIG+3nkcto5s6t6+yy9tqEc44Jm7AJm7AJG78JANDQ1ALHsRGORKAoHgQCXmTyeTimg2jAD4+qAoTg7QBZUaAw9QLimRySmg2ZOPAqCnLZDHr6+iBJEjjnoJRCUT2glJb2i4AyAp/fD5ExZAsmvIqIkEeB4vHBcezSN5C37eSKAkOumMfOPW0wNA2CIIz5Xs4BgREIgjPsPLMRp9wesfuj/Z5zDsuyMHN6C6KRCHRdP/TCCwJyuRwKRR22w0EEEbphwaEElm2COhS6aUIQJRRzSaieAJhIIQoC8ukUPKEwirmCuw8CBaMCbNuAwwFZDcBxdFimCW7aECUJoupBIRWH6vWhqJswtTyqa6rR2RdHQBIQiwQRz+WRTKYRi4QhyTK0YhG246C2pgamaWCwvw+BYBS1NVXo7OyEadmYPG0aVHDs7+iAZTuY0tQExgg62jvhkWTE6usQiYRxoK0DPT2doOzw94DjOBAEEaFwBJapY/euXVh+xhlYvHgxBgcHsW7dOlRVVeHUU0/Fq6++Ctu2oWkaamtrUVdXh4GBAaTTaaRSKdTV1SEajWJwcBCRSAT79+/HCSecgN///veQJAk33HADXnnlFdx222244YYb4DgOjj32WGzZskV+6KGHZh1zzDEBy7KaZ86cOaO5uTnc0tIiNTY2skwmw7du3Vrs7e3V1q9f30oI2d3f35+cNGlS2xlnnJHcuHEjEokE/vKXvyAYDOL6669HR0cHXnrpJZx77rnw+XxYt24djj76aDz00EM4+eSTUV1dje3btyMcDqOjo6NyD5qmCZ/PB9M00dLSglWrVqGvrw9Lly6FYRjYuXMnzjrrLLz44ovYtGkTCCFgjAEA4vE4LrnkEvj9fnDOkUqlsHr1aixduhTf/OY3ccstt6CqqgqO46CjowO7du2Cqqr4f//v/2HXrl04cOAAXn31VXz9619HZ2cnXn31VcRiMTzxxBM47bTTcPzxx+PJJ59EPp9Ha2srzjvvPMTjcWzduhUzZsxAU1MTnnnmGcSqYlhy5FJs2LgBr+3YiSVLlmBgYAADAwMIhIIoahpi0Sh2bN+Burp69PX3w+/zgjEGDoJMJguv14NsNocDB7ohyxKEibVjwibs7TFCCAghyGaz0HUd/f39oJRixowZLTNmzDglFovNaGxsXDxt2rTIZZddNtvv98sej+eQ7QQCASxbtgwAcPHFF8MwDBQKBTOVSrUpitIlSdLerq6uva2trS92d3dvTaVS+Z6eHsTjcViWBUrpxMV4KzzOCZuwCfvXGWMMuq6jt7cX8Xgc4XC4etGiRefOmTPntOXLly/51Kc+NUNV1X96+5IkQZIkMRQKzQAwo7a29tQjjjgC559/Pnp7e5PpdHpTXV3d6kwm85jjOGvKIFoG8wmbAM4Jm7B3jFFKkclk0NbWhkWLFnmWLFly8cknn3zl5ZdffnIoFFLejn2ora0N19bWnjJz5sxTLr300q/E4/G29evXr7Qs60/pdPof+/fvx5IlS1BfXz9xwSaAc8Im7N/rYfb398M0TZx00kkLZs2a9fEzzzzz3YFAoOrfvW/RaLT5jDPOaD7jjDOu7ujo2JPP5++pqqr6/f79+9vT6TSi0SjI21TPmADOCZuw/3IrF0XS6TRaW1uxZMmSY4444oivnHfeeee+U/d58uTJLV/4whe+zjn/2jPPPPNnxtiPu7q6NqdSKYRCoQnwnADOCZuwf21IXiwW0d7ejkmTJh1zyimnfPWcc845p/x7zvk7OpdICCHLly9///Lly9///PPP3/fss89+d8eOHZtramogSdLEBZ4AzgmbsLfWywSA/fv3Y+7cuY0XXHDBd4477rj3j/W+/wQ79dRTLzvxxBMve/zxx2975JFHbm5ra+uZO3fuRBFpAjgnbMLevImiiHg8jng8jssuu+wTZ5xxxo/q6+vlf9bDzGazIISY2WxWLxaLpiAIFAC3bRuBQECWZVkEIPh8vn/pcXHOIQgCzj///I8uWbLkmmeeeeYLjz/++M+8Xi9EUZy48BPAOWET9s97maWwfOZVV131i3e9613vGo+HaVkWEomE2d3d/UpfX19bJBLZuXnz5r62trbtoij27dixo9jW1lbs7e01VFWlnHM4joOmpialtrZWnj9/vprP56vr6urmnHDCCTXxeLxh+vTpzYVCYe60adMml4nnb8XxAUBDQ4N6zTXX/HTq1KnvfvbZZz/a1tb22n+aBz0BnBM2Yf9mY4yhUCggnU7jrLPO+shFF1102+t5Yel0GuvWrVtjGMY/DMN4+a9//ev6jo6OnsbGRixfvhy7d+/Gvn37MGPGDCQSCWSzWRiGUalsc86RTqfzkiTBNE3s3r27dXBwcE19fT02bdqEdDqNn/3sZ/TUU089cf78+adWVVWdNnPmzGMjkchbdtwnnnjiycccc8zOe+6550PPPPPM70RRxFsB0hPAOWET9n/9QREE9Pb2Ip1O46abbrrzrLPOeu/Q3zuOU+nIMU0T69evfzmfz/9l48aNT/zP//zPrksvvRQnnXQSLMtCKBRCfX09gsEgQqEQwuEwZFmGz+eDz+eDYRiQZbkCnOXXJUlCOBxGJBKB1+tFdXU1AoEAVFV1BgcHX1ixYsULa9eu/fq73vWumSeddNLZkiRdtmTJkmMU5c1TRiVJwtVXX/3bSCRy4he/+MX39/T0IBaLTQDnhE3YhI0dvra3t6O+vr7pF7/4xcNHHXXU/JHvoZSit7cXO3fu/J1t23/+1re+tfKkk05COBxGbW0tampqoCgKFEUZVTvgnzXOOURRhM/nQyAQQCwWgyAIu1atWrXrscce+8lnPvOZo2tray+tra29bsaMGeGhn3sjIXf5/eedd95VkUhkwVNPPXXOnj17Dvy3hu0TwDlhE3aY0FzXdXR3d2P27Nmn3nDDDQ8JguAf6WEmEgm8+uqrP37qqad+tmPHjv033ngjampqUFVVBcbY2xbWlsVvJEmCz+dDbW0tKKVrH3nkkbW5XO7mE0444fLp06ffcNRRR817o4A39P3Lli1beMwxx2y59tprT+/q6tr43xi2TwDnhE3YGKCZyqSQTCVx8sknX3XjjTfeMdLDBIBHH330jkcfffRmwzD21tfXo6GhAbIsvyOENAghkCQJsVgMqqqmV6xY8ZsHHnjgN0uWLLnkzDPP/PKSJUsWv4nzE/nJT36y4bvf/e4Zra2tT/+3CYdMyKRM2ISNEnYXi0Xs27cP3//BDz7x2c9+9o6R79m2bdu2j3zkIyf/6le/uppzvnfKlCmQZbni+b0T0w2NjY2oq6vDmjVrHrjllluOuPnmm69qa2trHeqxvhGLRqO45ZZbnpo1a9YV+/bt+69SXZoAzgmbsBGeZr6Qx/bt2/HZ//fZL9/46Rt/NvI9Dz744He+//3vz9+6deuK6dOnw+/3w3Gcd/yxOY4DQggmTZqEmpoarFix4s5f//rXLffdd99Xy7/7Z+z666+/Z+HChafv3r0bmqZB13VomoZ8Pg9N0/5PAupEqD5h/0XGQSkDIfQQ76oMGr29PSgUivjCF7/8+Rs//cnv2A7gODZEgaG3t3fgb3/723t27979XCAQQGNjIxzH+Y/r6S6DZHNzM2RZ5mvWrPnOiy++eP+ll15624knnnjqP7PNn//8508PDAwsWbdu3cbGxkZwzuHxeFBdXQ3DMP7P0ZcmPM4J+68wV0SYoVh0PaFisTjsJ5fLolAooK6+EV/96teuv/HTn/xBrmjAtB2IAsNzzz33zDe/+Y3ZAwMDzzU1NUEUxf8IL/NwZts2JEnClClTkE6nd3/lK19513e+851P/zPbUhQFDz300Krly5fXhcNhzJ8/H8cccwxmzZqFQqEwEapP2IT9RwInJQAINK0Iw9Ch6wd/LNNEOp3Gjh07cNG7L774E5/4+K8H40lQcCgixQMPPHD7D3/4w+W6bsQnT54M27b/T50b27ZRV1cHv9+P559//n9vueWWRa2trXv+icVJ/d73vvdSa2srHnroIezZs6fCR50AzgmbsP9AK88SIpTBNG3YtuP+OA403UCxqOOss85aeOkllzwQT6TAGINHlfHrX//6Kz/84Q8+WFNTg2g0CtM0/0+en7L32dTUhEQisfkLX/jC3GefffbBN7qdurq65u9///v3DAwMoFgsoq6uDpqmTQDnhE3YfyRwWhY8Hg8cTpDJ5lAoaigUNWiaidbWvZjS1Bz91a9/u0pVVei6Do+q4Hvf/96nn3nmme9On94CRVH+40Pz8S4wNTU1CAaD5pe+9KWLf/nLX95S/t14PccLL7zwivPOO+/KWCyGSCSC/v5+GIYB27ZhWdb/ifM0URyasP+SWJ3Atm34fT4IJVI6IQS5XA6NkybjC1/4/NNUEH3pdBoNdbX49a9v+9TLa9b89IgjlqCtre3/XHj+et5nJBKBz+fDfffd99mamprOiy+++NY3UnW/44477tq+fTspFArpxsbGDkppezabTScSCXR1dcEwDPT19UEQBAiC4KZMLOsdr106AZwT9l8Zrvv9gYo8mm3bSKfT+Pznv/CrZctOPKK3pwfNU6fiG9/85hfv+NMffzpjxgw8/48XEI1GUFdb+449rnLHUJl47zgODMNAOp1GWZDjjeYZbduGx+PBlClTcOedd/7v2rVr9R/+8Ie3jffzoVAIy5Ytu1PXdfz85z9HMpnMptPp3oGBgXQ2m90aDoeTJ5544n5JkvYPDg529fT07Eqn04VUKoVUKoVkMol0Oo1cLveOBNMJ4Jyw/xqzLAvBQAiGbULXdBzoPoCzzzjzvDPPOPvDBw4cQGNjPe6568+/+enPfvYDx3GwbcdOpJJJnHnmcvj9PhjGOye/ORQsBUEA5xzJZBKapkFVVQSDQcycORO6rsO2bdi2DV3Xkc/nKyD0emBkWRa8Xi+WLFmCX/7yl79qaGhgn/rUp37+Rvaz3BRQU1Pjr6mp8c+YMQMAlgLAJz7xCQCAYRiIx+Px/v7+tgMHDnQ1NjbuI4S0Kory2v79+1sNw2gfGBhAb28vBEFAOp2GYRj/Vn7oBHBO2H+NmaaJYCAIxgjWrV2D+sZG7xVXvveBdCYDVVWxYsULT/z617/+cHVVDIQSt++7RDtKJlPw+/3/ds+n7H0FAgEIgoBCoQDDMNDW1oZ8Pg/btsEYQ2NjI6ZPn46//vWvSKVSOP300zE4OAhVVdHT0wPbtmEYxpjeHCEEHo8H7e3t8Pv9mDx5Mm677bZfVFdXT37Pe97z+bfymCRJQl1dXbSuri66cOHCpQCwYMECAMD1118PSunm6urqjqlTp+6Jx+Nbtm/fvl6W5e3JZBIDAwMwDAP5fP5t9UwngHPC/musHH5y7sDQDXzg2uvu8weDkunYyA8Odt3yo1vOjg/GUV0dqxSCwqEQ+nr7sb+tHRdffCG8Xk8lfHy7AVMQBIiiCEop4vE41qxZg5UrV1Z0QimlEAQBjuPANM2Kd5bP5xEIBFBbW1v53fr169HX1wefzwev1wtCCHRdr2iBEkIQCoWwYsUKSJIEr9cLy7Jw/fXXf6Gurm7qySeffMk/A/hv1KqqqgBgYSQSWbho0SIAwIUXXghN09r7+/v3nXnmmWt37ty5JpvNbtR1vSsejyORSFSOYwI4J2zC3gToFAtFpFIpZNJppDMZHH/Su94zb9HSs7PZHBgBfvnzn52cLxTQOGnSsC4XAsCybciygpdWrwVjIurr697WThjOORhjMAwDgiBg586d2LVrF/L5PBhj8Hq9lQ6moYBuWRZkWa7wVcviyNXV1ZBlGfPmzUNraysKhQKi0ShCoVBlWwMDA3jttddQU1NTAVJVVSHLMj72sY9d+vTTT+9tbGyc9kaOY+/evRueeuqpvy9YsOCEWbNmNUqSVK8oSrAczo/nPDiOg3A4DMuypkQikSnz5s07JZlMor+/H7lcbmtra+sGURT/oarqi4VCoW1wcBCyJL/lIDoBnBP2fw4kLctGPp+DoxsI6GE4ji3W1NbMiVXHjmhuaTmqurp6dm1dzUnJZAJ1NbW4409/uHrFihdam5qbx6TMEErQ19eHRx59DOeddzaqqqrQ0dHxLzsOUmIBlDmQsixjx44dME2zkrP0+/0VMHlDi0ixWMlf9vT0YMOGDTjjjDOwdOlSVFdXY3BwEMlkEolEAvX19cO2X1VVhX379uGGG2445cEHH+wog/HrAVMp9Pf/5Cc/uTkQCOB///d/sW/fvuCCBQtaVq1a1eD1ehc1Nzc3NzU1LamtrZ2iqqp/rHNS3n/LsmCaJjjniEajCAQC8xsbG+cfffTR1/T39yObzW4/7bTTnncc5zFd01Zls9n84OAgTNN800A6AZwT9h8PlI5jIZ/PwbY5uONA9Sh1U6Y0LaqKVJ1Q01A3z+f3Lw0vDNa5OUoBAmOwbBP+QADPPfP0g3+59947qqpiKBaKwGEicEEQkMvl8Mgjj+OYo4+qANdbDZiO4yCfz0MQBDQ0NKC6urpS3GGMDVOHfzNmWRYURYHP56v0r4uiiJqaGixbtgwdHR1obW2Fx+MZ9jmv14uHHnqo88Ybb7zu1ltv/f14v6++vn7GNddc85GbbrrpVxs3bkQ4HE5LkrR+5cqV61Op1N9DoRDOPPNMAAivWLFi1rHHHrukpaVlQXNz89ENDQ0zFUWRJUmCJEnQNA3FYrFCY9I0DaZpwjAMAICqqlAUZe4FF1wwN1/I39DX3x8/lhz7glf1POzxeJ5oa2sbSCaT8Pp9/xSITgDnhP1HmeM4cBwHxWIBluV6QkySZ02d1nKM1+c7IVYdW+rzBWZ7PD5RklQQcHA4MEwbmWwOAnErseFIBL29ffn77rvnSkoBSgHLNA4PagBEgWJwoB8rVqzAkUcuhSzLbxkxnhCCYrEI0zQxadIk1NbWlsPSSpheXizearAue3NlorrP58OsWbOwfft2eL1eyLJc6ZqilMLr9eK+++77w3XXXXflggULThvvd11xxRWfXr9+/a82bdqERYsWYebMmaivr6/kWsPhMA4cOJB8+eWX1yQSiTWxWAzz5s1DT09PDaV00emnn35yXV3dUdFodKnP5wtEIhEYhoFsNgtd10sRh1X5yWQybm7b64nOnTfv4nlz5l48MDBQCIVCj+3bt+/ewUT8kXg8bgZ8vtJ5JRPAOWH/N4CSOw5M04Cha2BBCkbp/Kam5hMESTm2qqZumSTJzZM9XoiCAMu24XACywG4oYFSBkYBxgFTL8IBhaR6oSgy/vDbP7539569elNT07gI7rz0hz8QQDqdxourVuG8c895U55nOX8pCAJM00Q0GsXkyZNRU1NTCavL4PZ2GSEEmUwGgUAAxx13HNra2gC41e98Pl9RmLdtGzfeeOMlTzzxREqSpHFte/r06TNPO+20Y/7+97+/XPYak8kkFEWp8E1lWUZtbS0aGhoQjUYRiUSwZcuWvl27dj0lCMJTs2bNwsaNG4Oaph1zwQUXvCsSiZwcjUaPDIVCME0T+Xwe6XS64pU7jgPLslEsarBME1RgnoWLF106d/68Szs6Orq6urrujA8M/iGdTu0OBkPjOt8TwDlh77jQ27YtWCaFaeiggghK6exgKHJMIBg+KxCKHilJclOVzwdKGRzbAiMMlmnBNm1QxsAYUMimdNs091BGtnHHbjdMc/3WrVtbJzVOvvHd7774ql2v7Xymv6//70cddSxE8Y0/Bo2N7gTLRCKDcCgMQRRHBc9yDlBRZPh9PoiiWKlSV1fH0NMTwd69e5HL5cAYw+TJk8EYqwDmv8sopTAMAz6fD729vdixYwdOP/10LFu2DBs3bkSxWER1dTX27t2b/uMf//jl66+//rvj3fa0adM+bxjGu71eL7Zs2VIZWpfNZqGq6iGE/fLAutraWlRVVSEajSKVSqW3b9/+1OTJk59KJpMYHBxsPvLIIy9oaWk5NxgMnhKNRqmu60ilUpU8KErbLI1phuM4iMVijY2NjV+MDwx+MRarvvfAgQO3vrx27VpGgEAwiLHWwwngnLB/L1AC4I4Dx7ZgmQYct4Ld6PH6jguFI6co3sDpTBSnSYoKUVIAcHAC6IYBkTI4tglTz/dapvkaI+RV0zR2ptPpXXt27dzdWN/YW9fYCJ/Ph472NsiigObmqUekUkk88egj1xQKBUiy/E+KUBAoiopt23dgz57duPTSd8OxHYBzBIMByLKMcDgMQog7hiOVxoHubuTzecQTCXR0dOJPd9yJdDqNTCYN0zCgqmql4PFOWcTKOU1BEBCNRnHuuedi8uTJaG1tBWMMc+bMwapVq7538cUX3xiNRsc19vLMM8+8aNWqVdGOjo54VVUVcrkcHMdBJpPBwMAANE2DbdvD5jqNjEI8Hg9isRii0SgMw8DGjRvbJEm6devWrbcODAxMOfHEEy9ubm6+PBKJHBUMBpEr5IfRyMoeZaGQRyaTASUMi49YcsX0lhlX1NTU/nXXrp1f6+vr3en3B0f1PieAc8L+LQ+k49iwbQu2aYBIXoEJ8pFVVZ7jY3WNF3k8/mOYIBJBUiAxBodz2KYFbuXgWGY3585Gwp2Njm3tKOSyr/X19ewQRMmc1NgAy7aQSGaQTmcwtVmGJMkY6O/DSytewIKFC1tmzp41d8Mrr/xqcHCw2+v1VYoJ/1RIC0CWJWSzWfT09GNSQy1EUURbWzsGBgbx2GOPg1AKEIqnnn4WTz71DCilsCwLBw70IpVOQRJFLFgwr5JnfKdJsJXDXcYYNE3DwMAAYrEYwuEwHnnkEXR1dWHHjh2IRCKfu/XWW/84nu2Vxnh8/Lnnnrtp+vTpSCQSaGlpQXt7O1auXIlp06YhHA5DURRYlgVJksYMnYc2BMRiMei6jl27drWvXr36xytWrPjxokWLjps2bdp1/mDgmmg0wrLZHDKZzCFX0rYdDAwmwMGxaNGii5unNl28Y/v2mzZv2faNXC4Hr9czAZwT9m+wUvHBskyAO2CU+SRZOXHK1KlXKIp6mijKdaooQpQkWKYJy9Bhm2Yqp2vbwO1XVI9ndTqV3JPp69wuesN2KFIFQy/CsdxKqqJ6IEoSqO1WnWVZhtfnQy6XQ39vHyZNmYYZs+d/bjA+iMeffOxjlmVCsMw3DVSEEDBRxEMPP4pp05tRVVWNb938beRzORSKRVBKEQ6FYTtWyZt2940xhlqlZpgH9E7XrSyDVz6fhyzLaG1thW3bOO+889DZ2fmnRCLxw0gkUj2ebVxxxRUff/jhh2/q7OzEkiVLMGfOHLz88svw+/0QRRH19fWQZRmJRAKJRAK2bQ/Lgx4ORL1eL2KxGAYHB5HNZle/9NJLqx3H+fa8efM+XFdX95m6mlqpPz4IwzIBEBDiRj6MAqZlo39gAKIoYsnSo75eW9dw1lNPPXVFOp3eRymbAM4Je3vMcRzYlgVmGpAlSVIUz/mhqP9iMPFCQfUofo8MbjswtCIcXevIJuLrObdfYgJbl0smN+Uz2aykSqhvnATdMGBaJmRKXaEObsOC21Ez0iNhjKGvtxey4gETZdTU12PW7NnXbXjl5T8P9PfxSCSCfCH35vMMBGBMgEM49rcfgKz6oes6ZFkeRuMRBQEQBFBKKw/4UP5juQr8ZopAbn7YrYzrul4R+9A0rTL7p9w5lM/noaoqKKWglFY4o8ViEY7jVN5f7kQq06HKivm2bUMURWQyGViWhc2bN+PHP/7xV7797W//djz7Gg6Hq4PB4JnPP//8k7NmzYKu65V9KRd0bNsG5xxdXV3QdR09PT2VFMjr0bHKXrKqquVi1/6VK1d+yTTNXy07btk3Zs2Zda0shZBIJCrb4ZyDlO6d8jmMxaqPvOCCC7ate+WVMza++uqLLquDTwDnhP1rrByGC0yArCpnKv7gZVK07nJREjyMueGqpWvxVHJgDbft5yhjK8GdV/v6erkgMMTq62FaJihjECXZrY4zBsaEw4ILIQSUAJbjoK29HYQQ5PN5tLS0nKgbFtvT2vbBxsYpUFUVb4V/p2sawqEgCsUiCkUdjFEoigJznCmAcndPKBRCKp1+U8IVZcDx+XwnB4PBqCiKzHGc5xhjZjgcPl0QBKc0C0isrq5O6rq+pqOjI6dpGoLBIKqqqs6NRqPEtm3k8/l0Nptd6fV6KyBCKY1Eo9ETKKWmz+cL2rb9/J49e/oGBwdRW1uLhx9++PYbb7zxZ9FoVBnP/v7kJz/55RFHHPG59vb2p/r6+nLd3d0IBAIVr7KcvpAkCR6PB7t378Y999yDD33oQwiFQm/oXEmShGg0ir1797bffc/d15100kl/OPLIo34diURmDwwOHvT8qQue5UUmlUrC4/GoJ5188srunp4FO3fu3Or1eiaAc8LeYu/StuHYNhRFncFVz9UeX+SjgiyGCQiI7SBfyL9saoXnfbLwVyOf39rd3WNKioJQVQyMUgiiCEEUQAktDVazxg0agiBAKxYhSxKam5oqmpvZbAYLFi68fsdrO+/v6R8wIuEIdPvNwSbnHKIkwReKQFEl5At58DcIxa7XCRw40IM5s2dj2vRpyGSy/zR4Oo6DXC6HU0899dEZM2Z4AaCqqqpfEAQ6bdq0qjLlihCCo48+GplMJpHNZr/2gx/84Jfz5s3D+973vg+IonhheTsvv/zyjJUrV+65+OKLcfnll2P58uWPL1my5Ohy0cYwjPpMJoNNmzZh9erV6O7u5g8++OBvP/ShD90wnn2tq6tr/vznP/9AMpmMHzhw4MXOzs47k8nkg52dnTybzSIajVbeTylFMBjE3r17ccsttyAQCGDatGkIBAIYGBgYV5qDc17py+/t7X3xnnvunnv8icf/fs6cudckBlMwTKMURjjDIhc3x+nD8uXLn7YsvVHXdXtCAX7C3mz5AJZlwjZNiIxJ/lDkWm8otC0QqdsVqW74MmUkrOdyj2ip+PvyA11N+UT/sf2Dg1+xTGujw7nJBKHiUb6ZMNXlQdooFIrQNQMe1QdJVCAwCcFgBIqk+vu7Or8ZVmVIcCCTf/5HggOfJEDPpEAc603lJxVFRjyewNPPPIdUKu16q6Y5jMT9Rn5M04SiKH3l7c+aNat6+vTpVYSQimgwKwk5h8PhyM033/yL66677qyf/exnuPrqq98/MDBgU0oRCATw7ne/+zv79+9HVVUVLrnkkhOWLFlydBnEfvCDH3zgscce68lkMkgkEshmsyCE4O9///tPx3PcQxeHcDgcnTdv3oU//OEPH/jOd77Tduyxx34zEonUtLW1oVgsVjxQzjlqa2tRLBbR0dGBnTt3YufOnRWC/njBsyxgUtSKfMULK67t6uj4ZjgcAiOslPMs34fuvymlyGYzqK6urp0xc+bnBuKDEx7nhP3zHpdlupqIXp9vqeTxfVLx+q5isgJb11HMpR7Wctm7YJrPgbI4FRm4YYIwV+GHUOreoG8BsZsQAu442LlzB2zLAkq+HwGgaRoaGhpP7Ovr2TU4MLjD4/W9qSIM5xwu2dtBLpeGNxh80+cxGAwgkUjgiaeewbFHLf2nKVK2bSObzaJQKBSHgkSpv3u/z+crdyA1UErF8u8/97nPffFTn/rUE42NjVmPx/NZAD8BgEsuueTSW2+9tfqJJ57ov/jii28Z8lWtAwMDv5dlGQcOHEAsFsPy5csBAH19fXv37Nmzq6WlZeZ4j7/8N6UU1dXVUy6//PJvnHXWWZ97/PHH//fvf//7TV1dXZrP56sci6qq8Hq9SCQSuP/++9Hc3IxAIACfz4dMJjPu61sVrcJAfBC33farb51zznmxY4899uO9/T0oF4zKnmc5ZVAsFtDY2Pjp6qrqX014nBP2xh50x4Fp6ADnCIQjH6iqa+yJ1jWu8/iDV2m6+exAZ9sVZi4lFbOpCxLx/vtM045TQQBlAihjbwlQHgKaAPL5AmRJhsfrg9frh9fjh9frh6J4EI1WNeuG/iQRGcAICKP/1A8YhSBL4ARo72hFUStAFKQ3fQyO4yAQCCCfz+OlNWvR39+HYrGIdDr9hn+SySQsyxpKBC3+4he/OG7Tpk3NAJoty2pevXp1UzaX21B+Q01NzYLPfvazyuLFi3Huuefeunnz5s5yXvDss8++StO0qtra2iPLAPeTH//4Gp/Ph4ULF2LevHno7u7Gk08+ibVr1+Kxxx7DPffc86fxAGVlAS55yprmjm7O5XLw+/2eK6644kvf+ta3di1ZsuTc3bt3I51OV9pOHceB1+uFKIp47bXXsHnzZvT29kJRFIhjNCMMBeiyRF8mlUZbWxseffThTwwO9m/x+/0APwiWB4GTQtN0KLKn5tRT3nXUhMc5YeMKx23LAgeH5AtWR2vrb/KGItdRURZNw1zX39/zFdHB/Q5INpOIw6MooEx0Q3D2r1ubyw9ddTQGj9d7iEAHBwelVFAkuT+ZSq6JRqvwZmHbFQI+AE0rQFV8bxmFiHMOn9eLYlHDjp270dw0CYIgvqHhZmXlIKfUPE8IwbPPPnv37bffvmbSpEkVEOhob+9WZOWOpUcuXQIAPp/Pc/LJJ8vr1q3TOOdIpVIfA/AIAHzsYx/78rsvuuhj5e9oa2t7/s677loVCAQgShIcx0YkHMFJJ53kXovqamia9hCA7w4FyJHHWmYA0BJDolAoVF4DgFQqBdu2MWPGjMkzZsx4JBAI/GjXrl2f6+vrq4ielCvnoVAIxWIRzz33HKLRKBobG+HxeJBOpw/5blmWYZkm1r68FoNxV3Ju8qRJ6OrsxIMPPviJD3zogyvLhPyhfeuM0ZJItADbtv0TwDlhhzXTNGCZFgIh32lKIPD/FFU5iQKJbGrwJ5pm/FTxBQ7kMhn4FQ8EWYFwGLLyW22EEOiFHAbSCSiKOupDSiklHo/nJcMwiuPdL1LyrFGq6HJeEhNhAvp7O9E/0A9Jkv8lC4GqKtB1A/vbu1Adi4BRBnucIiKlajhs266g7dSpU3fdcsstmDt3LvL5PJ577jkkEgmkUik2xOM1Oec48sgj8d4rr8TvfvvbR7PZ7EtHH3vMMkGRIwsWLowAQLFYwC9++Ytr5y+Yj1AoBNu2IYgifF6fe844x5QpU9Db27tz7969HdOnT5888pqUK/9lNaZsNmt5vV7B4/FUFOzLOWPOObq7uyFJEt73vvd9NhwOBz75yU9+OJPJwOfzDfNaPR4PDMNAZ2cn8vl8xfMsb0tgDAaAeDwOQRSheFRM8k12KVCEIhSNIJPNvtjZ2bE1Gq2an83kS5X1gxoGhFJ4VBVPPLFengDOCRvVLMuCZZoIV9deG62JXafIaozb2Jbu7rzQtO2nLccGB+AJhCCUVMnfbivnNsu8wtGUbWzbNjnnpsfjgTOKkAcvb4c77v844ICASjK44yCRzpSq8xTcKSCVToAxAYxR4F/AV3dFLiRYloX+gQR8XnXcC5FlWSgUCrCHKJZMnTrVM3XqVADAyy+/jPXr1yMQCKCoFStorGua9cq6dWZ1LIZnnnsO9993H7p6ey75OP/kTkEWQ35/EFPq6rF61apvbtuxo6OmpgaJVAqKoqC7txerVqysKM4DwMDAAObOnfvYpz71qY+WlaOGKkiVRZdVVcXAwIB+4403XnDZZZddcfbZZ19FKUUul6vI6xFCoGkaDhw4gHPOOef6TZs2bbj99tt/E41Gx9QGKBQK2LNnD6qrq+Hz+WCaJlKpFGRFQTAcgs/vhyLLbiacVxZYJJNJxOOJldXVNfNBOOBGLBUQ55xD13UsW7asKLxePos7dqU5Hm9DZwPhHHAcOCVX/J+NrTjn7jbK+//v6MrgvOKt/CeY4zgwLBsgoMFw6GOEkPNkVWaZbPaJQjp1i1eU9GI+D6YoEEQRjsP/ZYDIRBGEDukSIcRtXyyZ+/2W6xFwB4IwtshG+YEeLb/KKAElBLZDYHMCwgRwDhBJgaUV0dkzAI+qgoOAwIYsUDDmeiLkX3bbuNQq23GQyxfBCMC5M+z4R48OTLfve7jUU2U3s7ksdu3ZBVGUEAj6s+XXW/ft7/vznXcW6uvr4PF5sOCIRUilkr1wnHRNTW0oV9CRzubwyGOPPhiPx+Hz+WBZFvL5PJLJJE499RRUV9dA13UAQFdXFxRFWQXgo+WRvyPD9jIINTQ0eE3T3H3FFVe8//bbb99w6aWX3lpKF1TAs3xs6XQaV1xxxW0vvvjiPdlsNuv1ekc9D36/H4ZhoKOjAw319QiGw9AMHaIsobm5+bAygKIoWq7Xy+E2ChFwh4LDgSxLyOXy5p7du/cIYwGmZZku1lA2y+I84B4rr1wFPlaIg1L/EuFDlvQ38NA4HDYIJ0zodhy927It2CWXezwxlpv3MmFbFogqBzll0y3HoQ7n3N0/d29GOw4yynGNebxljbER+zX0/YRzCtD9jm33lUOQw2AsBPb2BwDuzWnBNjgoY1W1saqrDcc+jglixshlf9Lbse9JzbLg9QbBPBKYILzpUNwFMxEOM0EIBWUCRFEGgQNCKByHQy/kYZsHJxlyx4FT6qwhAJLxQQwO9MPj9SIYjkEr5iAwhvFiOaUUjFKYloV4Oo1AqAqK6oNuFEAoAO6AgEMSBQgCAwcBBQEh9tvWGklLVBiHc2iaBs4dMCqMyRcte5wgpAKcDkDBOSgh6B8YwJat2xHw+VFdXZ3Z9tpOdA8MYPXKFwdeXvtyJU/c192HSy+/9IYjjz5qSjKdhsgk1NTU4LTTlv/sM5/61Ml9vX0QRdHtUjJNHPW+92H2rFlIplJlLxfZbHZDMpmE3++HpmnDQLB8/hzHgSAIOO+88461bXvv2rVr/9cwjPill176Z0VRSsfMK55nIpFAbW0tXbx48YWPPvron8cCzjLzwe/3Y8fOnZg2owWhcAiWaaJYLIwJHppWBOeORSkFcW+Cgw81IfD7/di/v/2l1WvWbBNGPry2ZcEwzeZIpOpGj+o/W/Yo05xya1ilRYwc9Aa5Cx7lG9qxSemLnBKMkuGXmR8KNpU3lF4WSBg1UUf35/NbsonB38Iyf1uWhhr6M/JhtEwLpmkg6A9cHoxUf9gbCC9mjITK9JTKdw/ZhzIIVLbHRyAoGbLfIAAvnVDKDx57+fOlf5c3wygFUX0pv79vESO0XZTEMYHEth04tvG2PZTl3I+maRAlubqqKna+JKstmlYsDnR3f9OR5a0KoyCUQZRYJWR5I942CHE7fgQRTBRK3j/gOBaKxTwsXYPjmCgWMrDMArhjwywW4XAHuf4uSJICX1Wt62nZBoq5DAom4BUo8tkUUokEIjW1oF4/Ent3QpUpBOpSnMbaV0pcj9G0TGQLeRQ0E7lcHv5gFRglAHcAvLNmeDMmQFY8yObSKJRAaLTjsywLhWIBxVLX0qat29A/2GcQQjFl8mRs2bodiYEETM3Ejh27is++8AIkj4KCVoAoySgUinht+2tomtKkXv+Rj/0IsgR/0A+ZisgWsrjqmmtPeva5589/4C/3Pjx99myg5Az89ZGHMWfnzkrBpiQovOvcc8/tC4fDNSPnIZUBtFy57u3trdmyZQvi8TieeuqpO0VRPO6yyy77aFdX1yHPvCzLCAQCS3t6ev78erOKDMNAMBiAbRko2iZs2znM8wAUinmIotjkNmAIsB0blJa/G5BECe1trb8oFnIuj9OdYWLBMgyEwpFra+rrfltTU89EJsLmFkCGYMlIT4sPyxbh4JvdHEGZLjLWg3VowqkCZjIBObJQnHxkvO/AB9OpbZdyx+lwvQoHtKTkzUtHbRgGJJGJ0VjNA1OmNJ/v8/sPAnwF2DBKyoFUKrCkfAaHeMkVL7u0YFQWIepup7yojOatioKIbCYT8ng8xxWL+XY2mu4j5yCMQnA49KLxlj1snHPIpTED5RBqlMqmNxwKLa6tq2vIa1q2t6/nu4Zlpg2tCNnjAaWksmCOfcNRAOQg3QgcTBAAasKxbVi2Aafg/m0bOhzH9VK6UvbB2rfjIG/bAAFkKgAgo6YB3Kqr+7ooSRAlCdy2YXMHHBSGzQHuuJ7aiGFqQz3MXK6AXD4H07YhCCpEQSitp+9MkQ3HsSEIAvy+IDLZFBzuQCvqh8C7bdsAIdiydavOJBmvvLIWlqGnKaXo6GhHf38fgkFXJk0SRVIVrUIoGkUhlWWEUEyb3gJKGK697gM/mT1rppTXinjh5VdW79uzZ+X1H//YFymj+PLXv/y7bVs3VXs8KjweD2zbcXUDSs9gmRva3d2N7du3vzpr1qwzy4A6NCU09LrU1NTMmD17NiKRCCilePzxxz939NFHX+P3+9V8Pj/sGEvjQ/qmTp2Kurq6w0YUnHPE44PY/OoWtwjE2Oj3MgHyuTyi0Sitr2843bG5GwlRXpG2CwQC6O4+cGBgMP7AvHnzXOD0ej0wDQOSIl08Y9bM3zdPnQbbNmEZBjhXDsILqeTPK/+oAGMJmAjIQfAZAiVDPzPU0xvpdGJITotSiqqqGGJV0aMsy3rZws4G07K5LIqlzigHKA2uIuBonDL96UWLlpwcCgVhGe4sEmeEl+UCIB8ik8+HxvmHAD0ZEce7nyIlH3aIB14KJ4aaqihwLAuCJM4KhAMQRWkMb9NGNu7SL96KaL3c7XCgpweEUgR8Plgl8Yehq7HP55MV1ZPK5fKrU9mMo+sGFI+K/BgTHMseAmUCCKPglvugggBGMQdqySCEIZ1MwOvxgxGKZKIXlJQeltLnGaWlc+zy4whzw3UMyfDQURZW9/vH9hgIIXC4W7FFiepCCYEoCOCcI53LoaAVYVpWaQyGAEIJ/hPMti0wJsDr8eOIpUdgest0ZNLpYfccB4djO8hmM4m1r6wFowQ+v4+Cl2bwyAokUQIIh6qqNBKOwB8Iw+8PMFlWMDjQj4vefcHkD3zg2g+niyaiqoqtr2751d/+9rc/X/qeK2+MRsPy4llzY9e89/1fuvmmm763+IjFgG0jnc5AAMG0adMqi3RJYORVy7LOHKmuPxRETdNEXV2dcPLJJ6O3txerVq3Ctm3b8pdccsnac8455+RsNnswry0IKBaLOHDgwGPBYBBjeZyEEJimCa/Xi2g0ih07dlTmKY3G8aSUIpvOYNKkSRdVRWsCiUQcjFFQSkqK8QL8AS/+9tBL1+58bSdqa2td4BRFCaqqspZZs+9aOH8BKKWuOorqK3kmB0HB4W74LQhsWGhaCR/IEM8Th3p4fHikDNty4Dg26JA2p3KuEpyDgKO6ahJkSarrGxj8+b59rR8XBBFMYKgKBcGYu6+qol41f+ERJ7e0TEcmk4IssdI+0IqHXNkmIW6ubihwljwrfph8p/u6U/JNyRBP1L1RKCFwykU1cCiKAsOy4fV6qxzbOESgwnE4BIHBF/DBLBjQtAKY8NaMnRVFEf2DA4gnE6iOxVBbXYOg318B0BIHLlEsagnO3ZtytHC8fB6cIZ67XsyBUAJDK0JPDUAQJWiWAUHxQhBEZFNJ+D1+iILL8xMkGZSNnjt+y1MQpWti2zZsxwETBGTyOeimCd0wIDAGsaT4859l7pVwHAf79uzDySefjOCcuYjH44cwGjRNY0SUoHpUSIzIcDiiVVXw+X0gAgElFLIq03BVBMFwFSLRiBiNRrB29Uv40hc+/7uA1wObA4bt2KtfWv23efPmo7+79+eNser/BwCfuOET3/3LfX/5xd7W1kx1dTWSySRqamtx4oknIpvNwnEczJ49G4yxvjI/c+j5HppqMAwDiqKIkyZNQj7vigpnMhnoul4YSkAvz2Fat27dK3v37t0cjUZHHdFMKYWu6ygUCmhqakI4HK542S+88EJJjV8ZIupBkMvmIMkyli9f/iWnxOQqi3yAMkxqbMC69Wvv3Lxl8zP19bVgguACZyqZQjAS/ujCRYvkxvp6xONxqLJyiD/o2DYkWUQgEEGuUITlmKCHPAJkND9ySOmIHKw4Ow4UjwegQDaRBFh5ayWvsPQvkTDMmDoVu2e0fCydjH9VluQkpRSGZUIzDHDuoHby5G/Mnj0LqiCC+IOVyLriFXPAsS34Aj4IooxcPl/K+dLDPYKjJD0PzTgc9KA5COdglEKSJfgDPsSqa/DKurWLDE2HotBhXpQoCMhlstDyRVDG3lL+Y5nuYds2DvR0Y2BwELGqGGpj1QgE/dA1rZR0p69XRYFEKYhto7urE5atwbHNoYkkCIGwuyhQBl7yDDhKlWDy72lOK6dPbHBk8nkwSkshOcF/skmShGwui5deXIMTTj0J0dpaxAcHDxbROIcnGJgpqV5IogiVoREgCIfDUL0eMImBgED1quFYdRWCgSg8imcWIwS33Xbb+84959zTAYAR4K9/feB3WzZvyv3of36IgEf9rqZp/09RFHi9XnzmM5958aqrrlpi27bFGMPq1asxODiIadOmoVAooK+vD8cdd9xr5557LgzDGJbXHLo4FwoFxGIxb0NDA3w+H+bOnQtRFDF9+vQjyv3vnHMEAgE4joPf/e53H2lra0NjY+MhYFxWwtJ1HSeddBImTZqEAwcOwDAMhMNhhMNhbN22FcWihlA4VGK9AF2dnbjq6vdfPa1l+pKuzgMuo8G24XCO+oZ6dHZ07Lrn7nuuMg0Tts1hWYYLnOFIqL62vv78SQ2NUGQJwUBweD6wdLOx0mq94dVN2L17dz8n3CH0DcY6vOJ5cse0bK9HrT7y2GPFpoYGZHMZEMoq4Vrl7baDWHUMM6ZNx6vr1h2v69ojoijB1s1yInrOtOZp0yY11COTTEKS/MND/xJPz+fzoz8ex0vP/wOZbHo/ZaJ46IN0ON7A67OPKAEVBYH4fL66SDSCUDSGqkjVTL8sQRxCmiYAPF4vtm7d7FJOKHvLH7Jye5nX460AaKI/iapYFDU1VQgHA9AtB47jdkYMVSEvDxDLpjNQVRUeXwCJYhFU4JWwGgCIWE74vnP9NOEtXpT+ncY5h8/vQzqZwoP3PYCLLr8Ik6c0IpVMVbpuGGO/E2XxCFmW4BWE5xyHw+vxQJVliEwAAaBI8tag1//HgM+Dutqazl2v7UR9fX0dgDsBWIZh4I477viqYeileUpIDA4OfrCxsfF4AHjve9/r3b17dyCTySRUVYVhGJg6dSqam5srkzpVVc0kEolSEwE/XFjtSJKEzZs3Y+XKlfjWt771wYULF9aWi0OCIKC6uho//elPv/6nP/3p1cmTJ2PPnj2HeJrpdBqyLOPEE0/E3LlzK91Htm3DNE34fD4cfeTRGIwPYt++fa5mqwvwNR/60Id+k8lkQAiFmwrlqKutRTqdHrjtV786vr29E7W1dchl3ZyrAACW7RxXU1M3f1JjvavOLQhDspK8EpZWVVVh9dp1+Mtf7v5+dazqWwDhMM03FHfZ5GDIrkiy/fLefXVtHR3PfuNLX5nh9arQDL1ESzqYtwEH/D4vJtc3wBsIHMNhPSIKIoqZQjlvOb9hUiNikRCIabizXoelBkohsT+Ivzzw1+SK5585vqlp8g7DsgXxLSoIEACWwzn1qoSJAunq7GwQCW2J+EMLVa/v1Ww6NazoJAoCiloRvb09aJg0BaZh/ksfNhdAPYAJHOjohiPKsKgA5rhXuFgarUqI69UYug4tnUIhl4MsSS5LQBAAYmHC/s3g6XCEoxFs3LABd/7297j8fe/F1JZpyKTT0IoaKMjPyhQeRt1UFSUuX5UeBKz1AK4lAASBQTd0JBKJ/xmaeySEoKmpCZxzV0DakW8HcHv5njrhhBNQphyZpolnn30Wq1atgqIoKBaL8Pv9iTlz5sDv91daR4eKNw/xonkul8Mvf/lLVFdXh6+++ur/LYf4iqKgpqYGzz333G9/9KMf3VxbWwtFUQ7xYHO5HCKRCI4++mhMmTIFyWSyQmMqWyqVwqxZs1DfUA+/349isYhJkybhM5/5zPOSJErxeAGCIMCybNTWViObSQ88+sjfj02n04NVVVUoE+IrwOlRPdNCoVBtXU0NspkM7FGJxEAoFMbAwAASycQdAmNaXW0tqCi+IXK5M6Raragqpk2b2lnMFx5PpVMz5s2dg8FEvFIkIEPAz+/3o6a6Gh7V05BNJ2DbBNy24VgWmKzMjUVjiIbCsHQDfGg4QElFhy+eSKC/v3d7TXXNDlGUIciwBMt5y4DTdByIHg8oY5Aleb8Est8oFp+xHMdV5LYdgLihbDaXRTKVAIE7zMuE+bZ4K4wyqKoCUIYD8SR8pWKPbdtIZzIwkwQSIyikemHbFkRJel3i9YT9OyrtrjAIJQ4e+utDaJo+DZddcQnig4MwdLMyddMZQefhw1kVpRqrU9EuHerBSZI0rFI+NJdqGAb6+/srXt7GjRuxevVqBAKBSveOqqqdhUIhLYpicLQwvbwfdXV17Mc//jEIIfKqVatW1tbWetrb2xEKhRAMBnHPPff8+mtf+9pHSoWkimddrsUkk0k0NTXhwgsvRCKRQDqdRjgcHjP/aZomGhoaIMsyzjjzjCemTZs2p72jo8Qlpqiri2DP7l27Hnrob6dSyrojkQh6e3uHbUsAAEWRLUmSeNDnJ+BkWH/mUOQMePywTRMNkxvzsVgMAmWwsgWAkvGHQiWqkkvsBSLhMAaSORGUIBIKQtPNEhF6+Pb8gQBCkTCMQnHK/tf2wOPzIVIdgyhLMAnxeCQJwUAI2YIGMqSWXnbyAoEAcrk84ECQRBmWySFIFKDkLWudG5rMZoxBIBQiYSCCAIsxUEIhiAI0TUMul4UkSpX2s7fbKAEkJoABoIyCUvcBKhRMGBRg4G9YZGLC3v6w3ev1weMP4K4/3QVDK+LqD1yN+EAC+jjmxL8l9xGlyGQyCAaDuPzyy4epFxWLxYJlWYO2bQdHPiNlK40I3tfb2xt98MEHX5w8efLsAwcOYPLkyeCc46677vp/N9100497enrQ0tJSmQBKKcXg4CBUVcX5558PURTBGEM+nx+1aDS0Mt/T0wPGGD71qU89Vd9Yv7ynpw8CkyFJEgJ+L3bteu25n/70f8+VZVmbN28B9rW1H7odALBMw6GU8kAwSByCUk8vOcTjDIb8EJgIj9crR6uisGwbGgAtnQMR3oBkGCEAZTBNE4V8DgXD4V6vB6FQALmihtGYn6FQEHKf7E4WVJWKcKlt27ABQ1FlhEI+pPPe0ucJhsbGwUCgPJqAcs7BCIFjuaGpQA/l/R2alMWwqv9wgnyJ62kTCKIAQRCHgWi5Wm07NizNQiqTqrxeDjlsyyXvH+LdjXCIrZHO3+v9fuTR2AIcy3Er/0PyqpyXO3oYGOEHQ4Myl8BxYJkmQMxhKU1qmrAtxb30ggUI0jD1G9PUQcgbWBhKx+PYNmxGDztbpvI77sA0jVLkY1UeXPdvMmJRo6XXXfI2oYBh6LDtUmvgG+x0G+kFOo4DQ9dd3UvGDt4mpAw0rNRFU1Jjr/CID12EOXdAKSsVMTgILSv0sMp3OdyBoqqoravFH2+/A6Zl4xOf+jgKuolsJv0vXZTL8877+vqwcePGYTOWyiryJ510Uk6WZWSz2WFhevlvwzDAGDvvk5/85FUtLS3VmUwGDQ0N2Lt3b9ujjz569fbt21+sqamBZVkVARDLshCPx1FdXY1jjz0Wy5cvx2OPPYZ8Pj+mA1f2dtvb2zFp0qTac8899+GWlpYjOw90gVKCcFUEoijiiccf/Z8XXvjH523bQbBUkBoVgAGXaiIIIoIBFdw5uIMjsc6jMDCRQtM0XiwUXW6Uz+N2oGTz468MEzpkuh8FYBFVlSFLAoJ+X4klORywAl4ZXlWBw7lT9tLcG5/AsTmRZRmCQA5+fiivFEA46EU8ocLhzrCuJ8IEcDjlVrVRuAEj+KaED+884gdvf8txIDACUhppS6grBGHbFkRRgs8noKOjHZSgVAxy5+OYhoF4MtWQSGVCkiTyw1XxUSFRvdEHwv0U5YTYls2RSLSJilwUFQWScuiIGLfLwu1oIgQoFgsknkzNIRR86CXmjg3ZoBnOHaL6YUbCYq+u64gnEhgcTNRnM4WwKFp8vG59edOO7UAoWprBEm3BoI+Dh1zFotJDYFkmCoUi0pksDF3zpNLZJkEQKSMuc95x3BZbMFZiKLlFzPK65HDOCSGEUJsYmo5MJtcDLgzajgEq0GEFsNfbY8e2oRWL0HUThUJB0U2rybJs1eHg5eehXEMtUfsIL3VckAqrbXjURgkFhzu8jIAQDs4ZY4TZNmFMMHRd79I0LVkQUFZ9R21dLe67+34kBuM4/5J348ijl0IlBIVCEbHqGLxeTwV436hXORoQMcag6zr27NnjNqAMKQKVB8WV8+tDtzE07Nc0DbNmzZpRBl1KKX7/+9//5E9/+tMXly5dajQ0NKCtra1CRC8UCjAMA4sXL8ZFF12E/fv3o7u7G4draS6H6D09PTjhhBNO+/znPn+PqEhVXQe64PF4EI1EsGf3nr4NGzZ8aOPGjY8wJiAYDB520REO5r7cK+hRFVi2dUh1ubxTjNAhmh8csB3Ifh8AAj2bA14PPEepwHIOyKV2REWRXI+RH4pbsiSUOnlGemVuT/Gwz494FAlxP48h+y4IAkAIDvT0IJfLuA8LcdyOynIrJsRhx28To3IRAYA5DAS01KlHAeKCPhUYLELgwIKhm4hFY6iqimBP6z6AUFDi0h1sy0IgGPzumWef+zmHc8H1RA5ySjklJWqPu+PsYLbDfZ0cVPhxO135GLBbhlsOQaDIZnP9XZ1dbZ379z812NN7V3WsavfQ5gOPRwE4gWHo4La14MR3nf7w+RddMqU8hKzSkVXyYyVJRiaTNh955O+3xKprqxctWnxaJBJtBCFiWaKtcrFfLxop7S6jFIODA53tHe2b97V33q0Viw8KjOmZTAaFQuHSGTNnXjeluXlONBpR/T5/zCnRS4YecrnD7KCnc3D5KZPfRUGAVijkuzo7O/e173uur3/wgVBYfIEctmOKgDscjmMhm9cjqtf/sRmzppw3Y8b0pvPOO7facRxwh1e+gxCXjI/Sglrmmw7dL0Lowf0jtNJFR0pRDisR+gkhyGaymdq62vb+vu5HC4XCr7OZTLssiqiprcbTTz6DNatfxplnn4WzzjwdkiSip6cPfb398Ho90DR93B5lGSB9Ph/cAknpWSyNON6wYUO5EDQs7TTMuTlEFPhgFFbeNgBs2LDhyT/84Q9fffLJJzc0NjaitrYWuVyusr1S3hTz5s3DsmXLIMsy0uk06uvrx7xGhBAMDAzA5/Phgx/84M2XX375Vx3HwcDgIOrrG2BbFl544YV7V7246mOEkGRVVRV6e3sxsmNpdI/TtkEoL50QAcIoo4jKQEE5A+UiZZS6T20pvFJLSeFiOgsqjADPMiWIsUNZn6UbQxZLwCkPB76hqM8ECgCkPDOl1CsB2JyLpbBTlcURYfZw3iRxXE4nYwzc4ejt70I2m4EgiOMmIfHX9UEIuO3A5A4sYsEsze82TdOd/S14AThub7Gel1taZn7pBzd/9e1OkVUXCoXqLTt2HX3vfQ98/fFHH/l6oajdbDoUXJAgMwIGHZTr6D7Q7120ePGUj1571VjbYgDQ1dXJZJF95SPXfxCzpk99q/ZzUk9f/6QXVq0+96577+94+oknfjStZdpxX/j8F64494zTEI2E3iptTG+xWJjVur991t8eefrjTzzx+O3pbOaD5crsQWfBXQQc24JtceRzxfOPO/6Y31/67ouis+fMQjDggyy+eVX4ockmPkrGSCtqAcsy5nd2dc9/+tnnPjM4GL9x92u7bmOCgIbGBmjFIv5y97144rHH4VE9MEwDjDFMnTYFkuSMCzRlWYZpmnjllVcgiiJisRjq6+uxefNm9Pf3IxgMwjRNVFVVwe/3o7OzE4IgQFVV5HI5V69TEFgZfEeCJ+B2NG3evHnnb37zmy8lk8m/O46DyZMnw+fzDQvNi8WiO+t8yRLU1NSgrNI0lpyhO+qiiN7eXkSj0SXXXXfdT2fPnn1cfHAQHMC05qnYuXPn/rvvvvtz6zasf2DRgoUIBUNIZ9Ljuj5CxXWueILkdV15p5JbOgiMDudQ/G6nUTGdBROFYTk8PsY2yzdGedi7cBjF8IMk36EFFVIZ5+lGZmPvu3vx3LBJkkQUNbdnWRKlISlMMiKJTQ4p/NCRK2g570kIhjrDhFAwQuGUjs3hgEAdqF4GEAmmqSObd2ZNf+tA5g2Zx+PBMUsX45ilizFn9sybvnnz98KqLHxGkkQQAgQCYdfn9uTWrHhhxc7r3/+e2ewwIWxtbR1u/eF33/L9rKupxnsuvhDnn3XG5Jt/eMtPzz7tNJx4/DFv+feoqgfzZs/GvNmzcdTi+R/42re/65eSycuz6RQESYIkCvCobmpKd3QICjvhjDNO+/vXvvJFeFR53IA4aup8FPX6oQ46HyHtKMsyvB4FwTkBzJg+TZ5U3/DL237161RnV9c9gWAQ/kAQsZpamKYJ23HgddMXyGY1iGLpOTuMR+3xeCreWiKRwO7du/GjH/0Ixx57LPbs2QNBEHDFFVfA43HPhyiKaGpqQiwWQzabRU1NTUlkI+izLMtNOYwATkIIFEXBypUrv7Nu3bq/H3/88TBNE319fZUQP5vNIpfL4cwzz4RlWchmswgEApV56aNhRDkHSgjxXHzxxTcfd9xxn1FVFZlMBnX19di3bx+ef/75/1mxYsXXWltb9erqaqiq+oa6yUotlyIVBGFcKXFKGWRZoYqigjvDu4QIIVBi1ZAkBblEErZjuXOxFaUSfo50ORkBJNOhZBwZecrLoF7qeRYYHDgQRU5LqazXqSRTCAKD6pFRLOZR1DWIouQWPSo5TDdU55VQnY4I1e3hlXAHbk7VKSU8h92MHDaxKyRcQgh0XUc4IsAfCKGQz8IwnJbJU5r+jbVZGwDD9de+H729Azf+6Q+3/y4W9u0QBQlQfHAoQ0Aqoqe3L5FIJBGLxQ5bsfwXlSEAcHg9Kr7/za8OqyPRt/ybHBBQnHH6qeju77/spm99628+j3qvx+t1c6wcME0LxOLCscuOvfdb3/giJFEeLcA5bA4XZPT/Y6zcOh/+ult8c0oSaiIuveRC7NnbescTjz38d8ApyLJa2iF1mBdmWjZ6e7ohiiJ8fl8Fr0veYeW9L7zwAnbs2FEJpQOBQIWiNGnSpENUynRdRzAYBGMM+/fvx4EDB7Bnzx4sW7YsKElSxeMc+uMOvQOSyWQyGAyiu7sbxWKx4unquo5YLIampibMmzcP27dvH3UcRhkwHcdBV1cXvF4vTjjhhA+fcMIJX6uvr2/QNK0yPfThhx++57nnnvv2gQMHdkyePBmxWMwViXmDRTQBAARRooyNrwNIEChUReCKJIyqfUgIQaQmCkocpOMpeLy+UljsjHpTCYRAlzilbHylTFEQCBNcJR6BlUIoSRhXMp8QQFYkyrmCffvb4HCOQj4PTdcrrZ6c2CNynNKwYrpNDdj2wRyn4DAQsIM5Tjq0cYDDJhZM3YZX8cAwwnA4kEmnUBUKQFRF8FDgqOOXHVvZR9OyQSgFo2OpSo14rl4/bwDTtCGL7DBRtvvEf/KjH8QTjz/+/db97eerigILHBZs5HIFzJs9y+RvoPvG4W5kQkcRcxltM4ZhQqnI7o08yBHaB6X9LYOmYVoQRyhPjdQcKH/aNG1IIjtsmqVsV15yIZ54/Imv7Nnbeq+syLBNA5l00h3apXiuvPba6+olUa40DgxXeh3BdrCdw0ZTYy5rTqn+QIYfyWje1kUXnie88PzTH9+zd8//hEKhMYE7k8li52u7AUKhlLbl9XoR8Puxfft2/OAHP8BTTz2FSCSCUChUAUhJktx22jEEo8sFodbWVtTX12Pq1KnnxWKxcBmUR3qcoiiW+aAbPR4PRFGEx+OBruvo6OhAOp3GGWecgba2NiSTSZimeUhoXgbM9vZ2eDweHH/88ZefddZZX5k9e/b8CmaIIp577rmntmzZ8q3nn39+TXku0VjHMm7glGWJMXF8lTavT8XuXZuC+/a9Co/iGT03QwDLttA8eTZUnx+aVgDGuGkct4eYCOMgWTPBbQN0SrAkMwGC7cCQRDKuKiil8Pr8dKD3AHK5rHj88SfcHopWHV8sakPI9m5VpvzAOy7lsQKGlIO7OvWcuIl+92lxzz+tyPvwkqweCOeEEwQDISudTKzr2LfnWnDL8HgDkFUvYoR4XlzxIl4wDfT092Pv/vZu07JNSogrHE0PKqqSMiKVH3CCQ3PBvFwrKu+/A62QN8NV0apvfOmzodrqmjH9oFAoiHeddtp5v7n9TxEwMcGggzsaRKZDkgVOxnGOi7qOu+69Hy+uWn3AcRxTFCQyVJW1JHTNAU7KJWXHcRzdMFBfV1Nz9Xsv8yyYP2/U6GTknfbCC6tw/98f7U8k03lVVd0EOD1YGuOjgKFpaNzn9fguOO/squWnnoKxW24BWVZwwrLj5j2/auUsVfW8JgkMtqGhoBUwZ+78K45eesSYIFYujOzcvQf3/uUBpJKZhChJnNIy3hNOGS3pf/JSYYgMrbATcM4N0+TRqmjkgvPOxqyWlsOe+2lTm9Aye+5Fre1d/2PYKHUNHXp4sdp6rHl5Hd5z2RW4449/QF1dPbxeL26++Wbsb29Hb28vmpqaIAjCmB7eSJpPNptFKpVCS0uL79RTT/3gVVdd9dm6uroGy7Kg63rFmx1ZLOrt7W0D0NvS0gLGGGzbRl1dHY444gjs378ftm1D1/VDznE5JG9vb0dVVRVOPfXUyy+66KLPzp49e+nQ961evfofO3fu/MELL7zwVDQarXRCvVmRFwEAJEmi4w2zXH1HrSqbK8DyWWPw2dxGeDaVlZRI7MPmHW2bsPHQmCilkCUJhDJQxiAJotvlRGU6npk3lBCoikKLRQ2WaR15xvLTr7ru/e8bd9FnLH+Cv87vy2Hl5778rRbVE/wAYxzt+w8gXzRRW1P11T/8/verc3njQH9/X+fCOdM6M+mkqRmmm18spVAc2+YOAIsM7+Io58LK+pW8JDJyEEwZ4DhYsyZ55cnHHXfX5ZdedNjjW7RoEQKe+5aG/OrTFpeQLlgQmQlVUYk4DhrLvff9FTd98+ZPzV8w75fJRNyyHWOYmjYBqShsEeLSbHTdQH19LTo79tZs2779sbvvuH1JNBw67FXY+dpufPgjN/wtFgu9NxDwFvcnU24erVRJKWso8JKsEynl5v2RKDp1w/vko4/ddv/99161ZPHCwx7PggULEItEj3ZM6zVJZLC4CMJVef78+Uv8Pu/rAsq3v/O9vg3rN54zu2Xm7nwxx0nJ7XS4wwlIOUAZBsC0rJ/HOTxelT/5ZGvLtm3bnvzDb38dO5zXKkkSWlpaFq1+cWVYFMVkORQebd/806Ziw9pX8JGPfhyhgB+5bA4D/QPwer1oaWmp8EQPd3y2bePAgQNQVRXHH3/8uTNmzLhx6dKlp3o8HvT09Oxau3btQ0uWLLlwmBrRiBpCb2/vq7quIxQKQZZlDAwMVOTgWltbMVSSrgy42WwWiUQC1dXV0rve9a73n3322Tc0NzcvGLp/Tz755COrVq36qW3bz4qiiIaGBiiKgr6+vrckpVPyOBVGx8ntYlSA6vHYgumq74xVkTNMNx8iyTJsxzpM6M9g2UQYD+uYEgpJlAgtzYmhjMFxKERBJOPjphGIgkhkRQahVBm5Iv+zMhDj+VxHZwdeWbv25w6TNY/PC61YBC9oKBTyKVlU725sbEA2m4HqUaHIAhLJlDsUTGAuFNg2RgXOSr7kYDGB0lIKg3O30cCwEDadZLH4+jQUVVHQ1Nw8Zd6s6TjQ0411G9eAgEORZDIeIZJdu/fmi8X8TwcG+hGNRmHb2pjeGCGEOw5HKOSKzGqa1ZfN9Dyyb1/bkuiSxYf9ni1btyKeSvyYUKfYUBuDwChM0xrai82HeuIU7syiom1jz65d+Uw6fcv+9s7XBc5orApBv39Opi+OcCSMSFUjstlcfSgUqn69c9G6bx80TVt1zLFHbeAWh8MdiB7JVcEvCWSPBpxlgLBtG16vijmzZm80NWPV/v0dF02fdvh8eCAQUI877rjJfp8veThnggO48MKL8Pxzz+Dxhx/CrNmz4fV6D+uNlReDgYEBMMYgy/KSyy+//MNTp05dXl1dXZXL5fbfcccdNwaDwfsfeuihA4sWLbr46KOPvnDodShvo3ysu3fvXl1WNNq/fz8KhQKWLVtW6RAaej66urowMDCAWbNmTV6wYMFVp59++kdqamoah9xX+kMPPfSXfD7/kw0bNmxav3493vOe90DTtErB6a0y1+MURSqOEzgFgUJgIrEtV15+9IvCQYkNURAhiyJsSxw7h0MJFAlsPPO3CQEESaQCE8AYAxUEUMeBIImk/PnXy8/LkkhFQYTABGvoO8ueG3ndBP+hubbRxOyHxc0geHXjFvR3dzwAcFjMgQBAlkWAUAiSDMssQpY41GAIcBy0dXQjEAxCM+2KtwwAxHa9S4eWyOAlRfTyIlAhkDsOCBy3OMUBURTSbBzn2LZNRCIhUlNbi1wxD0YFCEyEqijj8uoJpe25fA7btm3DrFmzUFUVGiWUPfjgKKoMXdPx2rbdiMeTmDy5kY3a8jvCMtksvD5F7h3sA9vN0NI8GbIkwTTMEWRy97tsy4JjW6iuqYf/yCUwDEPr6u56/fNh2QiFwn6/oiAYCiIc8kMUZcbGcS5KijxGIOBHJpOF1++FKIlwypMDALAhsoQcQDabhcAEl3rHGAhhCAZU2LadH895AedQVQ/3lriRh/NOTdNENpvFpMmTD+tdUkrLAsKwLGv62WeffUE0Gj2tpaWl2jCM/r/85S/f7urqemDx4sWpDRs2QFVVrF+/Huecc875QxfKkQsEADz11FMrV61ahVmzZqGmpgY9PT3DBDxM00RHRwds28Zpp512RktLy/suvvjiK0VRrFyAeDzecfvtt99BCLmtr6+v2+fzYdq0aejq6sJI9fm3FDg9qkRUaXzcMyYSiEQhpuOAwjM2cHIBInMpHJY5NnCanEBVMC7gJoyCSWLF42KiDAcEsiyxcmcGOSx0cgiyRCVJBhMFgQ1JT5BDypzj9zVdkQR62FLDnn37LNWjbvV5ve68Z0pgDSaRSQ0i4HPJxIV8DsWiJju23WxYVm1R00tDeIeEcZy74eeIYUl0hLyf4zicALC5wwHGBSJdXt9Q9/rXlxEoqkhFVXE5p0QEhQNFlMh4gDfg8woXX3IZvF4PBEFAf183urs74fP5D/E6FVVBsaihY28HLM2AzCiIKBBWUvY+3JVUPB6Ioiz5PF7k8kW0dRxA86QGSKIIo6TsUz5vlmmiUCxgxux5mDFnDiRRRNeBA9J4pnSKTIRAmVGwCrAcD2wb4A7l41lEPB4vAMK7e/rROKkBg/0DWLduHYQhwwdJpXhkQ5YkLFywAEVNQyqdRvOUJliWhXQqDY/fR0VZHEdEyGDbFjcMY0yOI+fuvm3cuB69vd0IhyOHLfgkEonqJUuWnPLhD3/4iEWLFk0yTbP4xBNP3JnP5x/s6ekpPvroo2hpaamMsti0aRPq6+tx3HHHnTRWDhgAUqlU1jTNDS0tLVi2bBkaGhqwZ88eWJaFXC6Hrq4uAJh29dVXX7pgwYL3LFq0qBKOa5qG9vb2Z+69994/M8buevnll52Wlha0tLRA1/V/ucZCWR2Jvd7go6EVKlGSKDN0VwV+jFDdcRgEJkAQhMPSVBx3WDwdn8dJIQkiBEYgMAqBMdgOgywrRBTEceGcqEjU4jZEgdnRqiroholioYDWfW3I5bLwev3gzhAVmUOuOS17VhgcGMDM6U2YOrX5db86lUrp6UzOTqbSkGS3516iDLZtIj4YRyAYnLXoiCWfmTd3zrlNTU114Wve79Je+Ch1Zj6i4HyYcAzgsAwbgYAfJxx/7Dhy2CJU1cNkSYJY6rl3CczCEK7vYTx6RaHz5s9DJBIBQGCZ87B166vY37a3MiyrDJqGbqBzbycs04LiUWAYGkRJJOOZ9ulGDe68c69HRSZXQFtXN5ob6iAKAqzS6AzDMFDI5zF/0RGYOWcuCvk8ckUNtmkxr9czjoWEQZFlWiiNXRAEEZIk0rEG7x0SIYmCWFdfh3wujxUrViAej4868kHXdXh9XpzyrlORSCSRLxaQSCUBhyMcDsPkDhsPWDPGIIoSdbm4dIyoUUShmMee3a9VNB9Gjz5sOI6jHnXUUefMmDFj+vr169cVi8WvP/bYY/qKFSuwZMkSxGIxVFVVwefzwefzYfr06aiurobf75/S0NAw5XD7umnTpuf279/vzJw5E0uWLMHmzZuxb98+5PP56kWLFp0xe/bsK4477rizh+Zq29vb9+/Zs+dvd9xxxx+am5u37t27FzNmzMDUqVOHydf9q00AANWjUmkcN4L7YKgQRXdE6esVdARJhCzLr3MwBIwxJozD42SUwqOoVCAMIhXcWTKODVVWiDSO4hYlFKIokkgkjPhA37pXXl697tUNr4Z2796znglsn2EYqVQyaQiiwHjpbiIHQ6lym5QDQqAVdCOTyx77ta99+app06bi9ViFPp+fMoEJxHaLN4Rz2I4DzoFYbc0Hz7vwgt+ec+ZyNNTV4t9pgksJobKiQJJlMEGA6DiQZGlcXBrGmPDSP56HLMuglJb4cxbSmSwCAX+l1VHXdBxoOwDHcaB61JJSP4WiqGQ8nUCKqkKURIK869F4VAW5QhEd3X2YXF8DSRJRKBSRy2Zx5NHHYv7Cxchls5BFCUQCdNXDFPH1v0cQBHehK+kLCIxBEEQijSNCIyCIxappT283nnjsceSyWQSCwVHDYs45JFGCZVrupAVRQqFYxLp163DMkUdh+owZfDzREGOs1Bc/tmIZhwOvx4tIJIpUKgWPxzvm1ExRFK3Zs2f/IZlMYuXKlaivr0dVVRWqqqogyzIKhTxkWUZfXx/+9re/gXOOwcFBLF++/JixaiBlu//++3+wbds2pFIpnHLKKbWLFy8+5Te/+c1FJ5xwwnnRaLQioNDd3a1ls9lHV65ced/9999//4knnoh4PI7FixejpqYGkiTBfpvUoEYUh2QqjjNUV2QZkiRTRtiYquVl3UdRECBKIkRDPExKhkMQRUrGUXhgjEFVFSKIIgRRhMgEONSEIst0PMUtSikYIXTG9BnIptKFv95331EzZs5G30AcLS0tqKmuRjw+AHFI7pYO8R4IAAcWisUCwqEopk2d0hGNRK4a13lTlNIwMbeJwCMrSKVSqK+vX/Thj3z4t1dcchHeCUYARKNRMn3adKQSCRDuLjiyJGGco+3Dzzz9pM+2zJyqelxRC8ogSTImT25EU/MU5HI5dLf3wHE4FI8ybICfIklEGgc1TpHlSsFKYG5t0ScIyGs6DvQNojYaRrGQx1HHHIelRx6DbCYNVopS3Fy3TOVxOAuMUUiSSClhYEyAJEmQLZOOBzgFUYBlms7qVasx0NOFUFWNKyYzipVUgpDL5ZDP55HL5aCqKno6O7GOUjQ1T4XAxHEBvSiKpDxDaqxnTlVVLFl6FDo6OmCa5qhRYamQY2azWXg8HoRCoQqRPJfLuXPemYBwOIL169dV5h/t3r0b55577vLD7WdnZ2enKIrxX/3qVx8Kh8PnLVu27IxQKFQ5qclkEr29vY/39/c//dWvfvW+008/vSdYWnTq6uoqup//rsmkpeKQRARhfMApCQJUSWGyII7Zk8s5BxzugqYsQjxMjpMTDkmSKR9HqC4wClEW4FCACwRUICAmIEkyOXjhyWE8TkCVJGpIImbOmYNcLlPyBn1u3rHUAjY0fKEjpnLm8gXU1zdi6tQWZNPZSQdbOujrPehEFETCHQcCJbAtV25t2bJlv7303RcOKzYdGnC/fWMfouEIOts72J9374ZW1ODzeFHIFyAJEqHj6NMRRUGsCkdE27KglDyOcq6st6unND7PnUkkDwHNUkgBjyQTucz5O8yxy5LkjoHgpd6u0nY8koRsLg9D03DmGWfgiKVHIZ1OuyOlhyyuTGSEjQP8KAVkWWaC6IEkKRAlAbItDwfOUYb8EUJgOza8Xs9pl19++VbK3uM/bITmin/YXo8n6zgOLRWPnIULF3rAuc1AamzNPFhF4MNTSeVpXYxRyLJMZVk+bESo6zqmT2/BokWL8corLyMQCI4YJOhUOJx+v38YqEciETQ3N+Peex/A+eddAK/Xh+rqasyaNRuCwDBv3jycfvrpFxzuvIbD4cD3vve9HaqqVhA7k8lg27Ztz8Tj8adffPHFx2RZ3rl48eJKV1IgEBim/v5vjczKeakynYc7TmWi4WgenySJEASBgBxevNgV7pCgijJscWx1c8pdOlT5TnDzYHzU7wahUCRXBISVQnd3n6RK1flwn3ccQJYlaiUt1Dc0IDF1GgYHBkCF8eV3c7kcamrrMGf2HBi6AcoILZPCLdvGaNPVy+dVkWVIjMJ0bNTXVMPn98Iy7ZZTTjllqdslxOHYHEMFNh3OITJ3wJth2WDC8HlMYyY2yfDGIkqAfD4P0zQQCIwtl8UYQ6y6Bjt37vS9+I+nEattwPw5c2HoGkRRIG5zJkYNi8rHKUmSQwgZVnYpV1UFkWGwL45gJAJBkYbPugdgEw5BEkGpMORaDqvFu9eeMQiiAEkUiVAKn4eBN6FQVS/mzl/g5hkFdkhnkWGatJwXH0rBqag+uUyEUouxuzALogBJkmHbDhVLojTgHKZlj0q8CIfDuPHTnw77fJ4wI8OlUw+RdC39v6iZFS0EzjlUWQDnQDabAytRrlzldj7sw4zQSj1BFCUqisJhB+U5jgOPR8W06S3YuXM7/H7/sHqjLMuVKndHRweamprhOBx33nknrrzyStg2x7333FepWmuahtmzZ+Lxx59ATU3NsQ0NDdGxnCpCCHw+X7BUEXe2bt36RHd391Otra3PPf300ztOPfVU2LaNcDgMv99f6Zt/J829d4tDXg8TSjeCphmjAichBB4PgyhKEASBO6X83GgJccdxUCgWIAoCAn4/LMMYE2QZIZAVRShTLTTNKPV8D3+f16vCcTg8skIFysAIrczNlmWpgieaZowKnB6P+3lJUQgrtWnNnjsP3Qe6sHnzdlimdUh74FDLZLJobKzHokULS73nDnw+HysnnnTNwEgdTQ5AkgUIpRAP4HBsG9FoDD6fH16v57RpU12Bj0JBG14FKhWgHFBs2b4NL69eE+/r7z9AAEIpJWSoXl75SR8aYpVeFVwJMsvmvPrCCy+oP/64o1AoaKPehO45thEKhWyAIRAMwIYNTjkEWeSmbUMUaOkcj4hEZAmCwCoVYz6asj0nEBURVKCjtuByOBBksZJY1nQDw5WACUA4vKr7PbIsE4ExiCOKSRZjUCS5EkGMdqy2YzNRFOE4gG6YJfHu4edTFF3QkmS3wUJgbl7dliQiSRJMx4FtuPq1w65D6SEXmACPXwLnQxqhhqAlGeGmEgCqJAyDV15C20DAB00zUNT0Utpo+MmzKIEDClEUIcsScSOosYGTCYKrpbl7F/z+wDARYlffU8Yll1yKF15Ygd/+9nZcd90HMGvWbDzzzFPgnKNQKMLn84IxBsMwsGvXLkSjVWhoaMCcOXM+PXbBjKBYLBbuueeevzPGnioUCs8/+OCDnVOmTEFZSi4Wi1V4nO8ksDwEOBVZoYxQ5IoaDF2DM3oTOqjo8iclUSyJwo7eapYvFDB1ajNEgSKXy8Hj88HQRydfM8eBIsvUMixohoGipo14mEp0HMZgWTYkSQYtgSZjZY9AorbNYRgWCppWEkIe/lw6nEMzdCiSG8ZQAJKiYPERS5DJ5EAZhTlGESudzmDKlEk48sglsCx3Yl5pRSecc2iWU/reQ50/w2KQRBmiJBJJFomiykimEujt68GSI5bODgRD0AwbhaKGkXrgiqqikMtjxfPPb3/owb8es3fPnhwbkbsawUoa9n9KCXL5HPK5Ai5898XX+IP+P+imjUKxOHqYXSroCIzKgO0WByS5xKSQqWGagGMfco0AwLTdayMKAhgTgBHitUP3SRQoRou2BEahKBKxHQ7DdFAo6kNaNctoQ1zPqlwc5IdKOvPSfVP2wEbL38myTBhjyBWKME2jIpLMh5xXQXRHhyiSBIHRUuHFLTCKoohiQYNpGO7zQoaH7eXrkM3nXT95lOszpts5yv9HCnuPvNkICETLLrEAFCaK4mFznNFoFJs2vYrWvXsQDkeGRRFOaUaWC160ImZt21bF+9M0DT09Pejt7QVjjJ1xxhnnL1q06Oorr7zyOM55rCzHN5rD9N3vfve4n//855vf/e5345RTTkF9fT3q6upQTi+8U8Hy0Kq6rFJGCJKpLEzTHGPHOQzLrRxLsghKySHAycGRy+TR3DQZxxx1JPKFPNr370NT83R4vD5oxeIhJ5IxB7KiUE23EU9kUdSKlQpr2QsBAM2wIDAGWZEJKY26EAQZgmBDVVVqGCYGkhloWvFgzuig5wVBM9yVVJKIR5KJIIocAIq5PJYdewxMy8aWrdugGwYoYxXZPL1YRHPTFBx9zFEV+X5RFGFbBKJIqW3ZSMTTKGr5cmA8PBXBCGTJhiRIRKAC4ZwgEAyCMQp/MOAt6jriiRS0ouYyFYacU91ykMlmEPD5dp911lm5RywLr732Ghoa6ith7FjASQhx6VUeD844/WzMnz+fFQsaEqk0CsXCqA+gyYGipqO+rtaaNr0FdfUNIA4gEAGqqJBiQUfOMWGMMpHTza2ZEJlAJEkkpslGlfhzgUwcNU8lCCJUxUMti2MwnkZRKxxMMg/hh5mmAwoCSZaIQzFETWBIMpu6IhKiOPrcJNlxvchUOgvLMkfECaVw3rJh2BZURYYADpESCKIEyhgRiIBUpgDL0t0IjQ93HyvpCSaWRHtdD9Mh/LA4OVp2m+PQIGpkoEEIgWA6CHj9iETCUrkYOnoeWoRlmti86VU3/UMPBaty55Lj2JAkd55PoVBAb28v+vv7UVUVbbrgggsuOe/8c6/8UO11i1VVQW9vj7Z58+bdCxcujIFj2KSE8vl49NFHn77zzjs3NzQ0oLa2dtg8+P8kc4FTlYk7dCkLy7QwVte2aVgghECWXDL00AfDbfTPoblpCpYuXQzDNOE4LqWhfX8rpk2fiUAgiGKxMAw8ueNAlmWq6wbS6bS7+vNDSbO6pkNV1Yqn4AInARNc1XfTspBKpmBZ5vC7q1zkcYclQ5IkJkoSEQTBHWtQAuloJIjp06bC0IsQJREEBJquYXJ9HZYdvwy6rsO27YoUlqvuIlHHcZBKp2AaeukuHzFyhBKYil0KYxkYc/1h23YgCIJpGAY0TXO93REdFlrRFZTwBwNKVTSK+vp63Hrrrejq6kJDQ8OwcQFDgbM8QItSirPOOgdVVdUwTVPhAFKpNAxjdO/f0A1Yto36hgb7qKOPBiECerq7wUs0paKmoVjIj5rjpJTC8rgjWFwlHDoqcJZn7oyVupFlGZVzaho4JKnLCUzTAlxZMsLooQBNS7lvN2yVR91fQgmllCKTzcIeCqxDvk/XXVCUVZkyJrhMDlEAIFBGGbLZLGzbGK5Ni4Nz6TVNQzw+WFLyYnDAK2Ov8QbnvJMR+nOHyNMRCp/Ph64DXQP33nvvjjJAjlaEAqGwLBPJZPKwOW9K3YhxcHAQ2WxWnDp16gnvf//7zznmmGNO9Hq9UyhlvL29fdsf//T7G+fOmfPEK+vW7aqOVX944cKFv7K5U2op5cOu92OPPfZpr9eLcDiM2tra/0jQrACn1+thhBCkU2kcrq2rWCxCFATIikzLN2x5Rcxmc2iaMhmLFy+EaVoVRRNZlqFrGlr37sKcuQsQDkdGDFUiUFWVaJqGYjF/SJpgaJWyvL3y64Lg3pCKqlDT1FHI5eG4+m4V/Ko8B9QdpCbJMpEliTJBcMphgav/Z8HQCqiNVVVEV23bBmUMiXgcVbFYyUsqNVESAlVViGVZSCWTlVDPIYc8f7BMl7YhiIxQRlwlekKhqCo1dAO5fK40EmSEYAghIIxAlmWWGBxAY2MjvvSlL+F73/suurq6UFtbf3AoGT/YepnJuGyBc8+9AFOmNKGrswMNjY0EnCOVSMF27IP47hwEa0opautr0d6+X77nrjsxbfoMzJo1F7quQ5YkqhWLSKfdAWAjGwMId4s5jDHCGCOjgSOj7mgIhzul6zTcLNuCJEnctm2kU0nYY1RPD96HCqHsUOBkzFXRkmV5TOAURZGCEKQyaaCkszkSYCilUL0qVEWl5ZBfkiQwSiljzB2lbVvDquoHj5VBkiXs2bvnhR07d34lHApSQjioQwGMQw2wLFHHh6edaFn+sSwHSwkpz9by+33Cs08+uW3dunWDwWBw1O3atg1FkRGLVVVGVozmbeZyOei6NmX+/PkLWlpaFkybNm0uYyywc+f23jVrVv+urq7uxQ0bNu3o6jqAbdu24OU1q0Epw2f/+OdbAcByrBJf+aDQ91NPPfXYxo0bdx5zzDEoFot49dVXsXDhQoy3+eadl+NUVQpCkEymDmapcWgsQUBQU1MFSZIJLw1Kc4fB5zFpUiMWLZyHsozUsAqdokArFrF92xYsXLQIwXAA+VwOlBBwTqEoEjSt6AJqObzgw77YJQVLIiRJImWPwvU8KWRZJrphIJ5IutqfZTVMAtASEDsEkBUZoihQsbQNwOVXAsD2bVuQTCSgejyVQkFZfn/z5s1YsHAhGhsnHZzWBwJV9VDLtpDJZErHS2CTsngDqYTulsXd6ZGlqYfu8CoGWZaIbuhIJJNuyDIs2nP7mT1eFbIkM1lSMDiYQE1NNW666dv4yU9+jNbWvQgGQyXg4pXEvs/nw/LlZ2Ly5ClIJOIQRQmqqjDbtpFKZdzrBg5OSuo8pVjQdhxEqqIIlNojVUUtEb8JRElEvpBHMpkEobRyfstELHf+jptXHCtEdEGeQGQCbDJKqM4YVEUhhmlgMB6vTAUYDVVqYlWQJQl0lFx7OY1UDtVH87wopUQzDcTjcTA6ZKTLwbFLsB0H1aIAWZYr9xxlFIIgEsoIUqmUC5xDH5hym4RtIxgKoqG+oT2RSKyOxwfBuQ1FVN+Uh3VI3njIsXPOkc/nK9Xokd/jOA4kSUQsVjXkeowSWZomZs6ctczr9R2dyeRBKd1hWfb9L7744u577rkLS5cuwaWXXobq6hgch6Ojox3PP/8sPvGJG25obKhViprhLpDgAHFA3ZE3WLly5QemTJkCWZYr9L9ya+h/ZKguyTJxHI5UKgFChRGewsHV1LFtVFVXgUkit7kDi3Pk8nlMaqjDogXzYJkW9FEq6G6LnYpioYjNm17FkqVHIBz2lwYiUciKiIJeRDyZgCBIo4Zxtm3CH/RDEARSTtKX/1ZkGbphYDCVglKetFem5VRoSjZ8Xh+CXtUl7hN3PAAHsHXzJiQS8VJv8fD9Lk/u27JpE0RRxpQpTchk0jBNE6rHywzTRCLl7ndlAmUl5CZwuAPTseFXZSIwgdDSqA3ucEiiCN3UkUgmIIoSOB+SN+aAaZkgzK0MM0EEpQy6biESiWLRwoUQBYZAIHjITe/1ekujC3QXpBmFLCvE4g7iyWQl3TBSsUbXjZLQr1gBAcYIKCOQJIpCoYhEKgNBYId4x7ykxCQJjFNC+Gjz4svCFiITQDGKF+g2BlDTtDCYTEKRlIPFQXLwvFi2hVgsClGg4I51iGxh+bXyAjUakIuiCM0ykU5l3NRCaYEeek7c0Q8hyJLsDn6jFIIoQpIUAgqkUknYDj9kpDMBYJg6BEmEIon++uoYIqEAunp6IAsyWGnhed3wfESEPZT2Vb7HFK8HhUIBuq5DkiS4RSE25uJV7ugyh/Tzj7znbdvG/PnzX5Nl5aXVq1eDUorZs2fD4/Ggvr4esVgMsVgM+/d34NVNm/Dart1onjodn7jhkze795Hu7gcnIKVb6Xe/+903Hnjggb6lS5diYGCgMkuoo6MD1dXVkCTpP9DjlCRS0A0k02mwITPBR4bL7rhPB5IkEkLctrkpkxqxaO5c19M8DO2Icw7Vo6JQKGDDuo04YukS1NfXw7IMqKoXqf5BJBJJqB7vMNVuXsm/aYhGo+VQqTJylLldKdTQdSSTSXhUdfj43tI2ygWCaMhPKCVE9XhBCMGmVzcgHh+A1+sbc79lWYYOYP0rL4NzBzNmzgIFIEoy6RnsRyKZhqKoBwtSQ3KVZepSyFfr9no7bghI3DEERNMNJJIpeFTPQYenNAlRKxbh8agIet2hVFVVUXDO8ec7/oiuri53St+IMNTr9aJYLGLD+lcwf8EiBAJB6JoORVGoYztIJJPwqJ5S+pcPO8/5fAGmZUIqhU60lC8tV5KLxSKSqaRb/RxStOAlmhVlDNWRMKFjhOrlFMhooFr2gJgocss0kUqWWgHBDwERrTR2VpQkl+0xgnbjTpSkJU7jmFV1ZIsFpNNpiCPI7OV7p1goor6+DrJcyqVSCgYCSXCPL53NwhmRjy+fl2KxgFA4DK8koVgsonHSJEiSjPXr1rkaAPSfG/pBSkNmylM2++MDqK2rgyrLYJDh9Xqg60VIkjBqmE4pEA6HKqLBoz2vhBDk8/m4bduVMbnDRFMsG9u3b8dfH3wQnHN4vSo+9rGPf2nmjOnBgcEkGHNFhimlUBUPtmzd0v65z33uJkmS8PLLLx9SvZ86dSpmzpz5trdNvmng9AeCUr5/AIPJVKW1brhUWulGKhbKxRzLsm00NtRhwZw5MC0LxmFAcygIeTweGIaBbdu24r777se+fa34zne/Z0mSgsFkCn7TOiTvQghQKBRRlc4i4FGpLCvu6lr6UVSFaFoR8XgcxogQpTyGVdPdqvXMqc180qTJTiabwdq1a5AYHITP73/d/XbzMBxbN29CoVBAPpfFosVHOJKsYjCegM/nLXkFwz0k07Rg2TaaJtWDMTfHR5mrPykrMinqJgbjCfj9Zlltt4ISuWweAX8QLVObeWjOLAzG47j37nuwb18rYrFq6GNQvARBQLFYxNYtm7Bo8RLXa5JlatsW4slEqXDnHFJ0SKfTMA0DsiwNe4gocQsthUIe8UQcqsczlH3j5ictC4Qx1MWq3PEmjI1RHKJjV5EdDrlUBR9MxOEzTWBELpQSinyxAJvbkBSZEEZBRnSdua8xiG5qZ5i2Y9lUVQFSwEA8Xhr8hSFFSXc5yWaz0DUdkiQTQihESUQg5IfXqzIOjoFEqjTW4uARce6AgCCbz6MqVo26qZOxcOFCxBMJbN64Ad1dXaXe99cvDo3Md7v/sF05OirAti2sfnkNTjr5JLRMa4HIGHw+LwqF3CGE//J9XCwWwBirKBkdbh75aNcuFoshkUjgrrvugm0ZKBQKiISD3gsvvODb2VwBhmHC5cc6iETCAIAf3/Lj871eL6ZPn36IMHGxWKwA6Fic23cscOqa7jgORzKVgSy5VKPKaAZe5pwRt+jAKQI+vzRl8iTMmD4duXx+TLd/TPItozANA7lsFtu3bUGxUFQUv4REMgnHLg3G4AfzqgCQzeVKakEO6erqQFSrwvTpzQDnUCRZKmgG4vEEHIcf/PyQ3Gy+UIDP64Pp2NIjjz9iMUKQTMShqi6Qj6+y6XqKm1/diNt//zt84xs38bMvuhQ9vb3uELNR2ACG6fL8CAgYZeAOgUf1QFE88Hl8Yi6fRzyRcgshQ46Zc45kKoV5c+ehp6db++v990EURezfvx/RaNVh284451AUBcViAVs2bUTjlCbMnN2SdBwHAwNxOPbBTpmD9CeCeDwB07QgSzI5CJoElAKiIJJEpoBEMgWfaR3Co9F1A4IgDptmOBYQMEJgj+rpuPxK3TQxGE/CZVwdpCO52Y9SJdxxK/1urnVEaogM4XuOweNUZAWcw/XAdaMUPnMMjVOSyRQ0XYdXkYgguEwO27EhCG7SbiAeh1LqWjs4ldK91slUCmQmRSFflB7629/gcAtt+/eVJoeOz0YHTlYCTlYi+KvgHCgWNTi2WzhjTBhzAiQhBL29veCcV+aRv969xDmHz+cD5w4GB+OIRCOorasDYwL6+/pwww2f+H0kUkV7enuhKp7S4DcJlAB33333VxPx+Ba328ge1btNp9PIZrOQx7mgvGOAs7Nr3/5odT1000C+UMRwiTYXhAgl6B+IY197J9594QWfNrXcLklRs5ZpUTpi/s2oZLRhm+PgjoPlp72Lf+AD186bPX/u0X+6+35omo0UcsNIsOXxu72Dg1AkCfls6kBb2x5YloZNGzehf6AfS5cc2VndMBl9g3FXo7NUhCDlDC13E/3bduzCSSccF5kzq+XD27dse6yxvoGDuJMKRiuGjbb/nHNURcLWZz/z/2LvvujCq1asW49sXockF0Y5WAeW7cByCAjhRGAiEUQJ+9vakUwmsXTJEouqCvrjSQiC6BYaCK2c72yuiMFUAlMmzZ7+4ooVUrQqakSiUWQzmVHqsofS7yljiMcHsXfPbpx3ztkzuaCgt28Qgqi4nT1D30so+uMpZItFiCWPk1AXNCkFJEHimXwCg8kMbOfQi63rBmTFTQGMFY5X+v/HyPFRSt28r2EinsyAULHiEVc8QUIwMJiGbjhQBIUKnEHgwz1bwWEQ4IrQjOX5SrIM27IRT6Rh2RTuEA9ecRgIIRhMZlDQTUT8HhIMBjDQ14enn3oCTc1T2q+57sMACAYTqYM54SFW0Ay0d3Zh0bxZR6588YXJnFsdUyY3j3HtRruOfAzgLNF8iNv9JooMfp83Mn36tIaG+saFu17b8Xw+n+8e2gl0qOPC0N3dDcuyUFdXN2yM98jrRSlFJBJBa2srXl77CkzbcRtGKEVnZxeOOmrpKUcedcxlvb194I6bY7dtB3W1VXjyqWee//SNn/5O05QpiCcSYx63prmjM4477rhKsfY/AjjhkC1Bv39AEoVYa1s7wpUJedwdf1vygmwO3Hn/ffjAVe875sr3f+DVdDJVuc58qFcAHKKKzg/Ogql4sZQS+IJB3P/3h/Hqli2IVdWgUMwPwzBO3LCkvbMTlAHV0ZrN11xzDfz+ANaseQX9ff3Yv79tXfOMmYjHExAVdz5yRTG9DMCUYnBgEPfc8yCuff+Vv1q8+CjHsEzHRSoM8WrIoeXVISF4yfu2IpGo9NKatXj4kUcRjkSQSCUP5oKGAIZuGAiFwqCCAIdzblkWmEChemTIstBJJRndvT2QZVcaiw9J8Dq2g0eeeAIN9TWznnru+dbWva37GKXCiK8YCzfLr1mUUp/i8R/xp7vvhWYZGEjEK3SgoVSk3oF+aJp2EAg4KqONBVFAvpBH78AA7FI4Wl6UODi0ogbZo4AxRoQxQvXyKB3DGr2t1zANUIE6mmGgb6AfRKCHDHQiIIgn4uDcLvWSH9qhBOJW95umNCEc9kHTnRGFIQpG4XDOkUinYZTvl1IbJCktXH397vkQWMRxHAe5XB67d+/F3j17u9733mv2BAO+lq07dqIqGj24SAzhVT713HNoqK2uefjRJ3fv2rnzFYEx2yUhkNdd+BwcShHi5eGAnINw2IQQAZQ2ibIU0Q3DWxuL4ZJLL3t585Ytx4bDYRxunAznvMKAGY0OZNs2IpEw6upq8cILK/HY44/D0HVEIhHkMhkMDsYRDAXwoes/fL9pusLDsizD1hzMmjUdbfvb+7/yla+e7fX6AcKgG9ZhPGuCeCKFbDaL15Oie0cB5x13/Pn5j3z0o88sXrTwygf+/hjmzZ0D2yx1U5CDTW+UCSgWNXzrB7egeVITFFmCya2DIFdqs6qELJQMy/2U5ebKvxcEAb19fUilUqiujmEwmSjlupxS14/LldF0EzbnaJ48GVtefeWJWCzmeieygFAkiC3bXn3uvddcg6rqauza24poJFziv5U5ly7QCJKIf7y8Fi+9+iomTZpEHcemZc+LjBJClNf9iidSeh9jTMrmcjjQ3Y1wOIx4Og3OHTilwk9ZvYaUCgwgDJRRGotVcdO2UFNbjULOg66ujpVLjj4BRd1AZ18vJEE8OB0TboW9oBXw3VtuxTFHHdVYXRVrtAwT484CcYAKFA7nWL12LQYTCVRFqtAfjw93qEuFnP4SBYgyZgLu+fL5vNBd3iQv6Br6BweBISIUtHQts/k8IlXREvFcsLxe7yEPgW3ZUFQZ/qCvlJIZDhuBoA8en4flCnn09PZDUlT3vimtvOUyUTqZBCOAIDJL8ciQ1eEVWcoCIAy49y93QfUcmoopFApYOH8BURQViXQKeU0/JCdICNA9OIBsNotoJCI2NTVBkmQEQ2H09HQjFqte3TxlSsvtf7obbK4ApyR2wYfWBSjFT37xG7zw0lq5Zdr0E0zDgEPs171mvLTQY0jKqayxyUv3liC4DR2FXBEDyTi2bd2Gd19wPo5eOLNEx6I47Myh0vPnpuYobMt0i2qUglKCpqYpaGvbj388vxLbt29H+XrGB5PQdR0Dgz346Ee+d0d946Toge5uyLIEbhqYVF+Prs4ufPCDH1zW1dmuT506DZZtHZbvLwgCstksWltbUVdX9x8RrgsAsGbNmmxTc/OtX7/p5itv+83vsb+9A7W1tXAcG3xo37dpul6i34uuvm53XRzhpQ1beIfOgh76i7JHxQFRlBAKh2FobqHDJgfvXEIACoqd23fipq9/GaostX71y19/yeP1wuPxYPL0ZkRi1di7d3eys6P9L9e9/72Xv/faD8HrmVua5c4rLW4oeTpevwembWNv2z53gBfBqOHkoS1tZYorBYcDSoFIJIi8riHT34dolUuc55aJoU6r6ThIplPgDqehgC/S02X27W9tRX88jj179jx/1rkXDp72rlOq/nT3PZg9aza4aWLoRHWBCeASwbMrXoRpGmCE4Y3IzLk0IYJQKAhKXdDxeLywS40OQ3uz7dKICa/f55cVBdFoFKeffjoK+TzCVdGa7p4ecMc5uKiW9oQQAm47SMTjIIwqkydPCRc1Le33B0ZQpSz4fF7U1sbcltERKwAlFLFolbq3sw+aoUMzDBA+fDG2QFAwNFi2Ba/P66WMQhCFYdtSVBWaVsS3vvVNaFoRwpBQmgAYHBjE9R/+cPDqD3wUuqZBFERY3KlwW4dSuyzLhOL1BIzSiNvu7gPoPnAAf/rjH3/6+S9/5eo/3fMX7Gvbj8bGhgp4Dl18fWE/Nm7bgtXrXzms6MZQxCYlRsPQlmFSytsSyivnHIRAYgJkVUU8k0LT5EmQRHHFwoULUVt7+DEplFBQRrBt206kEln4Q35wbkPXdFDK8Nxz/8DatevR29ePxsmTSnl61xPt7u3B+9537fsuvuSSq7q6usC521UYioWgKBK+8pUvnb1yxT/2zpjRgnQ6Ma7xsYVCHj09PYjFYv85wBmLRfHXB+5bd+rJJ9x19x9+895zLrkSe9vaUV9f465IhAx7WCkjUDysdCOOVCoYrfuWD0fMEWeSA6jocZKDvb7pbA49Hd14/+WX4WNXvw8f/8RH30cEEdGqGDjniA/EQUFQzGm49ZZbbvjr/2fvSqOjKtP0892tbt2qeytVlaokBJIQEpC9bQUVRFxoGkd72tZ27bG1PTNtKyjDuLc947FxOaOn3U6P3Y1jjxBxaRhWtcem8SACsgkCIYEkLCEFpFJZaklV3Vt3+eZHVZYKAeLonAPtfX4mqdyq+3313Pd9v+d93mXLb/rlIwu4F1/7D/gDQfgLfL3pXv8HAM9lDSJw2vPdQRkor8+NMEBbewc8soQ5V87Ett17wTo59BemUELBiw6cjESQUHVMmDBp6vHQyfrKYBCk4SDq6+vph2tXzf/1E48s3bRpM5oaGjGifAQEnsu7XxzLwO/zn3JgMBQdIGUAgEH4+AmUDy8BIQy602mwPJ9XmrAAcKKIo80tuOzC8ZddOHky1GQKT/zySUy77FLfZVfOGnYy3A6H5MqtVV8DCwUgSBKaT5yAQUAUj6eiqfHQ0dLhpXnvJ51Ko6SkGKOrqpAZ4IlACIHsksGxwviT4QgEpxOUAQjYPMsWlgBpXUdXIolAIDi5pSX0To6++2p4HAs9kwEoA4YRwODUlHXc+IlVsVQa8ZQKxefLf+Dnrig6nYglkzgZDo9Yu+YDFPh8iMejiERasWLFn3ZdfdWM3y5e9Pq86358GxoOH0bpsFI4BK5ftpJ90MoeBbJHOUNNpe/nPRpPYmVLWehtC87uh2xUSHqJE4TAsigKvAVQZCeam48ekGXljLrIrNs8B4dTQnssCUvT4OUD0A0NhpUBJQTLV6xCgSKjuKgQpmn0knVnZxtmTJ9Wfe9999W0R6PQ1Aw4joOnQIEiubF61cq7Dx06/OeysvJc1jQ02RXb6yB2HsmRol1d6OjsxJNP/uof3lpSU7Dm/Zrr/vDHJdix84usfCWjgyAX6RALg9doBv3anoZET91AfR2A2XqU2+VCWckwzLv7Ttx6449Q8/aS23d/+eXWC8aOAS8IoJaJRCwO07IQKPRj3759kbnz7r/iNy+/+tG4sWMKlry7DIePHEGiI51L/XMpdP8oeMhUlP+3lmXBNE0UFRXhuScfw9UzLsX3fvQT7PxyNwoDwQHVG4LjoRA+XvcJfnb7zQ+9+6flS2OplKFpGQQCQbz08ivvFA8bPm3t+zVzn3/xZWz6YhcikSgMw+pN+wd/MA3+3gZCNw2wYDB9ykX43Wsvoea9lXj4icdRVFICwjB50W0qlcKyZcsxe+b0y2+59dYJn27cWMseY3HFjJkP1u8/yG7csAHFw0uR7O4+dT0Jg+ajR7F3Vy3mzJ5z5/p1f93g8/ryooeMS4OiKDCpkTNv6bt2PJHA1EumVEkFnitWrF4FNZVCe2tbLvXt+9wMWEQjXVj7wYd4bP682+bOfeBXDMPq/c1RWIaFbuhoaTkMXdfzan0WpUinkuz0y2fc/ebSZWgLhyHwXD8SJ3mHFsuX/zd+OHvWpKuvufqSDZ9u3CYrHlBqIRJpw8MPP/LAb197veiD95fe/Pqbb2HL9h3o7IpC09SsAiN3X4a2s2ifcgN9HelMTxNHTwpPetQOTE7TmbV1E0QWwaJiNO7fU69p6mmd5rM1Xg6pZALbN2xGLN6NYb4CUGoBNNvhRhkOYydMRDLWie5ELHf/KDo6orjo4qmOxx57/DOLUkS7onA4eBQU+OH3efDMs7+ev2rVmsV+vx8O0Z1NNIcYmPCChM6uOOLx+OA99ucicfKCkB26FAji4QULrp/zd9fe9Yu775h/43VzLowmYkintWz9jrDZGzwk6Q45RY85FFjUgkNwQBIFlBQFY4RYf3n0oX9+jueFL0eMKEM4HO5XKGd6Rdp+vx+1+/d//shDC0Z/f861Tz/9+EM3hSORYCzaDVVVwbDMoPZeg6a2g51m9vt99jRTwJgx1di1ddOLb9TVOn/z/MJ5X3yxC26XBPS7DgiBbhgoDhYi3BauqKvbx6qqaiiKki2Kd3TihX9/YZ6WTu2Y+493PXrTDdeP6+iMQlW1PGH32e7jYGYflFIYFoXH48awYGHbivffrZlUPfb+d/5zkTOtpsASJu+QhmEYqFoaLqeIurq6YaFQS+306ZejpKR01KiqSrz2wrNwu+V8A2Kar8mbOH48Gur3FXE8C0Ly3Z4Uj4JIpA2bN3+WMwLp+13ryZO4ZtaswjnX/xCP/ct8ELC9B3t5n58wMEwDkiggo6llVaNGsSBEz1+mbGmovKwUA7vIKaWglsVaplF+7ayrcMVlU7PlEJo/n56QrIG0llEhuyXE4vGyWDy2TVZk9OqIeQELFz59y2133P7Tn958w4M/mD3roq5YAhkt06uP7Cv50DMGDhhYc+75PvQfvtqv4sWQrB8DSxmYhoHC4kIkop3prdu21TMMMyhxUgrwHAtqWYh0xOCUXMhkzDxPgKz8yMrpjJ2QJAkMIVDVNCTJhRtv+vF6l6wUtbe3g+d5iC4JLsmJp576t/vffe+93/n9QaialrP8G3qOlO1mMs4Jd/ehn6rnoii32w1CCJYtX7FYEJyLvV7PyH1791wyedIkryA6GFM3ewYVnKlEc+r+6Bfg5dUMBxbFCQjncDAnQiH1RPPRhnHjxu1qOtKcqK+rx/dmz0Y4HD4j2VVUjERbe3vkmWefvf/BeQ8sOHEy9B2OF8ZXjqpyZ7SMCYtSQs+coA/hsJo4HAITOtyUrN27bWvtntq6LZs34+FH0zvGVFV74/GoTvNqpxSSKLLh4836G4sWfZhMpjWPR4FlZYXwHo8HJcNKsGbNB4tTaW1xZ7RjnJbsvnjSpMlKJqNbPacNZMA8WSvHeOQU38Z+tUeGEAfLMvt27mtZdrDx49ra/emJE8ctuf7vfzCTGIZlGBbtH8lQwhCv7OZee/mlg/tr9/1FdDixetUH2L1r733zH3xg4/DhJVIsEddPM0IeBZLEbft8U7qm5q33GIb0Wfz1RL8ZDbqegdPpPEXvOaKsDDt27Nj63MKF19xy663j090JyzQN2mvT1yuSoFRwcMRUNfLcwjc27tq1S5Xd7gEb6swPx1g0mrl4ypTJt91xx/d5Shkjler1NyJ9lqiUMIR4ZZlb8l9/bF68+K1VAEGopQUOhwOBoB9uyYWiYDFWrl6zpK2tbUlV9ejypsbGqSOGlxbKsswahpEb0py3kfJWjA5iF2f2f5jlOoT66pzZ15s50alFQASOZyKho8bqVWs+bmo6HFcUZVCBBc9xSKXTSGlZ023BKeF08+FTyW74lAKMHT8ByUQMuqFj5swr3i4fWTX9+IkT4HkektMJjyzj1VdfubempmZRdfVoEIaBYXz1WeY9KpvzSsfZnzx7LPMLvF4ca2k+srTm7SNNU6dmD3AyGgD2K0c+gx++kMEq1uBEEXX1tTBUFZWjqlFYGECB1zukJ5FhGHC73SgtLQVhWW39+k+2tZ5s3TZt2nSk1WyHAvO1GxMIRJHHl7t3o3xMNcaMvgB79+zBmjVrl1RVjISaTuYZ2WZdzQXEY3GEQqHcwyk/kiSEIBgMwuP1Yv2G9XU7N22qm3nlVTAMA5T2yYUGrtXZInuGYaAbGTQeasLIUdUYP348Gpsa9769tGav2+mGaVj5M5UIAcuzqK8/AFl2wTQp3G4Zum50r1u37o0Cr4x0Sh286yeX5sWS6WxHU26cwkD9YE/qPPDzMAwDl8uF7du3f0Kp9QmhADX13v3WW4ulFjgHC1XNoLm5GaWlpYPuwezMnE6YpnWKsz/Hc4i0tR1auWLl6wwF0E/T2jM7PmveS8DwLFpaQnA4xFzLItMrCu+/doqnAJH2juaVK1c0B/1+lA4vRSaj965fn+yL9Dz4Bs90BnhjMrnspedap/hXMgQ8z6IrGkVXNI5gMDioGTDHMDAoRfhEK3iOQ8Dng2ZapyW4npqqqqaRSidx4XcvejNQVPqTjq4uMIQiUFAIapr4/LMNN9XV1a3w+X3geA66fv60TX5jxDkYiRaXlMBfWAhZUXodof/fiJMQsKIDRUXFULsTX9s1pWe2t8frhag5vzHidDh4FBUXQ5aVnBs8j0AgAK/PBzXtOJU4BQEOhwhFUdDR0Ykz3XNZllE8bBi8Pt83QpymaSCaiEMQBBiGAUVREAgEIQoiLMPqP/4YIFkXJJ/PB11Xc19WQBQdKCwshKy4IIpnIk4OvFNFW9tJpJP6oOR4phIJISQ3ejaQPaE2jd79lkecQnYaQKStHZqmDaoXBQBByDZDDLymRSlcbjcKCwtBTSuPOHvec8/rOIFHMpnKzayyTivzoZRmCSkQRKDQjwKvL2cKnk+cp1uvr0WcHAtOEKAbFkzD6DMCAcCzWT+HSDSGbjUDCtLrnH+277GeySCVSmLCxIl/qKwcdU80lp0UWja8FO3h1sifP/rohpGVFVuKiopwqKnxvPTV/L+CgQ0bNv6mQHMpPs8QdKdSSFkUGiWwLBPcEE65GYZBOpVCOpXC+AkTfl82svLnse6s5WNRIIBjR4/sfP755ybt2bt3S3Fx8beKMG3itGHjbxQiz2XTbMqgozuFVEaDwHO5nvqzk2YsFoXodGL2tdeuKi0dcW8ingLH8ZCcIrZu2bxo0aJFUzo7O1t7JHLfRnD2NrNh4/wHISRnt0gQT6bQ2Z2CqPjA83zWVGUIqbllWWhrC2NkZWXFhVMuXV1cMmxSZ2cX/IUBmGZG3bLp03/atHHj2y6XDJ/P19tVZxOnDRs2zu00PDfPSOD57Iw6QsBy2UNAi1IkNB0t4TYIDhEyQ2AMkXRVNQ09o6KysuquaTOueJ0VnFIinoBHkdHUUP/xwYaDP7cs41iBpwAczyMej36r18EmThs2zhPCZBgGHkWBaRpoDUeQymSgg8LBsAgdDyGR0cCLbvAMA55lh0SYuq6jO5NGsUcpHjNm7Aslw8vv1EwCFwBT1xL79x16aOf27W84JAkVFWVIdEW/tVGmTZw2bJxHhEkIgUeRQQjBsZbj2Lu/Hp2xKCRvARinCIXh0d7WBqfHBRfHnlUL2TOIMJVWwfE8W1leuWDk6Av+VXC6FEoAnhA0HqhffPRI0+OCILS6FQWCw2ETpk2cNmycJxGm7AalwLGWEOobD+HosRAymoaiogAkUQQrinAyHJxO8azyvZ4I09I1yJITxcHg7aUV1c/IXqWSZwkIdISaWz5Jdcefamw4tIkwLEZXj0Jra9gmTZs4bdg49yNMRc7OwGpuOY4DDU04ciwE0zQhu12QciQ5VBBCYOg6UsluSJJT8Pq8P6uoqPqFW/F+hxcdoHoGXR1d6zrbI680NDZ85Pf54fV6oWqZ86YF0iZOGza+xRGmomS9AI61hHCg4RCOHmuBaVqQ3W6wLNM7hXIo/49SCiOjIaNykFxShdMp3VMyomKuWynwMQwD3TQQPtz8XiIa/T2h9NN4dxyi6ITL5YKmZexFsYnTho1zPCVXFFBQNB8L4UBD06CEOZR02bIsaKoKhuUAQjxKYXCO6BDv8foDs52SC9QwkOjq2NXV1bGsK9K63NQyTSalKC8rh9PUYXR322m5TZw2bJzjhCnLsHoIs/EQjja3wLSGRpg9TkbUsmBkMuB4Al4QqotKR9wiKcqNLpf8XYdTArFMaMnk3vaTJ9ZGO9pWS5K0I55MoTsWQ6AwcIqDlA2bOG3YOCfBEAKe53Hs+Ansrz+IIy0hWKYFWXZnzYzPQpiEEI7juBKG5UYLojSxuKz8asLzUxySq5hjOFim0aqlku90HW/5H8PUNztF5+GT4TDUdBLl5RVwShL004yXtmETpw0b5yRk2Y3auoP466ebwPM8FNmd57x0JhAAFuDgBfFyUXJN7YxFA5LobOJNuuH44aZ9pml+rkhSNyM4EOtoByNwcLlkOEQRoOag7kk2vhqIfQNt2LBh4ytmDPYtsGHDhg2bOG3YsGHDJk4bNmzYsInThg0bNs5j/O8AP7GG1dsy9nkAAAAASUVORK5CYII="
	},

	/**
	 * Helper Functions
	 * @namespace
	 */
	helperFunctions : {
		/**
		 * Formats the given Number using OGame's language specific decimal and thousand separator
		 * @param {Number} n Number
		 * @returns {String} formatted Number as Sring 
		 */
		formatNumber: function(n) {
			if (typeof(n)!=='number') {
				throw new TypeError(OGameNX.dictionary.getPhrase("ERROR_PARAM_INVALID_TYPE")); 
			}
			else if (isNaN(n)) {
				throw new TypeError(OGameNX.dictionary.getPhrase("ERROR_PARAM_INVALID_TYPE")); 
			}
			var /**@type String*/ sRes = n.toString();
			var /**@type Array<String>*/ sResSplitted = sRes.split(".", 2);
			sRes = sResSplitted[0].replace(/\B(?=(\d{3})+(?!\d))/g, unsafeWindow.LocalizationStrings.thousandSeperator);
			if (sResSplitted[1]) {
				sRes += unsafeWindow.LocalizationStrings.decimalPoint + sResSplitted[1]; 
			}
			return sRes;
		},
		
		/**
		 * Compares to version strings (positive integers separated by dots or just one positive integer as string)<br/>
		 * The Comparison is based on the comapring the numeric parts (in numerical way). The strings are not comopared lexicographically.
		 * @param {String} sVersion1 A version 
		 * @param {String} sVersion2 A version
		 * @throws <ul><li>TypeError if either of the paramters is not a string</li><li>TypeError if either of the paramters is an empty string</li><li>TypeError if either of the paramters does not match the expected version string pattern (positive integers separated by dots or just one positive integer as string)</li></ul> 
		 * @returns {Number} <ul><li>-1 if <em>sVersion1</em> &gt; <em>sVersion2</em></li><li>1 if <em>sVersion1</em> &lt; <em>sVersion2</em></li><li>0 if <em>sVersion1</em> = <em>sVersion2</em></li></ul> 
		 */
		versionCompare : function(sVersion1, sVersion2) {
			if (typeof (sVersion1) !== 'string' || typeof (sVersion2) !== 'string') {
				throw new TypeError(OGameNX.dictionary.getPhrase("ERROR_PARAM_INVALID_TYPE"));
			} else if (sVersion2.length === 0 || sVersion2.length === 0) {
				throw new TypeError(OGameNX.dictionary.getPhrase("ERROR_PARAM_EMPTY"));
			} else if (sVersion1.search(/^(\d+\.?)*\d+$/) === -1 || sVersion2.search(/^(\d+\.?)*\d+$/) === -1) {
				throw new TypeError(OGameNX.dictionary.getPhrase("ERROR_PARAM_INVALID_FORMAT"));
			}
			var /**@type Array<String>*/
			aVersionParts1 = sVersion1.split(".");
			var /**@type Array<String>*/
			aVersionParts2 = sVersion2.split(".");
			var /**@type Number*/
			nResult = 0;
			for ( var /**@type Number*/
			i = 0; i < aVersionParts1.length && nResult === 0; i++) {
				if (typeof (aVersionParts2[i]) == 'undefined') {
					//Version2 does not a version part to compare against Version1, thus Version 1 is higher
					nResult = -1;
				} else if (parseInt(aVersionParts1[i], 10) > parseInt(aVersionParts2[i], 10)) {
					//Version1 has a greater value in the current parth than Version2
					nResult = -1;
				} else if (parseInt(aVersionParts1[i], 10) < parseInt(aVersionParts2[i], 10)) {
					//Version1 has a lower value in the current parth than Version2
					nResult = 1;
				}
				//else is not required here the only option is that they are equal which will
				//force us to continue the comparison in the next part
			}
			return nResult;
		},
		/**
		 * Retrieves a URL (GET) parameter from the current location by it's name (case-sensitive)
		 * @param {String} sParameterName Name of the URL parameter
		 * @param {String} [sURL] URL, by default the current location
		 * @returns {String} The value of the requested URL Parameter or <strong>null</strong> if the parameter is not available  
		 */
		getURLParam : function(sParameterName, sURL) {
			if (typeof(sURL)!=='string') {
				sURL = location.toString();
			}
			if (sURL.search(sParameterName) === -1) {
				return null;
			}
			var /**@type RegExp*/ oRegex = new RegExp("^.*" + sParameterName + "=([^&#]*).*$", "g");
			return sURL.replace(oRegex, "$1");
		},
		/**
		 * Returns the Array of Sort orders
		 * This is an array containing Objects with the properties "option", "dictKey", and "direction"
		 * @returns {Array<OGameNX.helperClasses.SortOrder>}
		 */
		getSorderOrders : function() {
			if (!this['_orderCache']) {
				this['_orderCache'] = [ 
					new OGameNX.helperClasses.SortOrder("DEFAULT", "ASC", "PLANETLIST_OPTION_ORDERS_DEFAULT_ASC"), 
					new OGameNX.helperClasses.SortOrder("DEFAULT", "DESC", "PLANETLIST_OPTION_ORDERS_DEFAULT_DESC", function() {
						return 1;
					}), 
					new OGameNX.helperClasses.SortOrder("NAME", "ASC", "PLANETLIST_OPTION_ORDERS_NAME_ASC", function(a, b) {
						var /**@type String*/ sName1 = $.trim($(".planet-name", $(a)).text()) + $.trim($(a).attr('id'));
						var /**@type String*/ sName2 = $.trim($(".planet-name", $(b)).text()) + $.trim($(b).attr('id'));
						return sName1.toLowerCase() > sName2.toLowerCase() ? 1 : -1;
					}), 
					new OGameNX.helperClasses.SortOrder("NAME", "DESC", "PLANETLIST_OPTION_ORDERS_NAME_DESC", function(a, b) {
						var /**@type String*/ sName1 = $.trim($(".planet-name", $(a)).text()) + $.trim($(a).attr('id'));
						var /**@type String*/ sName2 = $.trim($(".planet-name", $(b)).text()) + $.trim($(b).attr('id'));
						return sName1.toLowerCase() > sName2.toLowerCase() ? -1 : 1;
					}), 
					new OGameNX.helperClasses.SortOrder("POSITION", "ASC", "PLANETLIST_OPTION_ORDERS_POSITION_ASC", function(a, b) {
						var /**@type String*/ sCoords1 = $(".planet-koords", $(a)).text().replace(/[\[\]\s]/g, '');
						var /**@type String*/ sCoords2 = $(".planet-koords", $(b)).text().replace(/[\[\]\s]/g, '');
						var /**@type RegExp*/ oRegExpCoords = /(\d{1,2}):(\d{1,3}):(\d{1,2})/g;
						var /**@type String*/ sG1 = sCoords1.replace(oRegExpCoords, "$1");
						var /**@type String*/ sS1 = sCoords1.replace(oRegExpCoords, "$2");
						var /**@type String*/ sP1 = sCoords1.replace(oRegExpCoords, "$3");
						var /**@type String*/ sG2 = sCoords2.replace(oRegExpCoords, "$1");
						var /**@type String*/ sS2 = sCoords2.replace(oRegExpCoords, "$2");
						var /**@type String*/ sP2 = sCoords2.replace(oRegExpCoords, "$3");
						if (sG1.length === 1) {
							sG1 = "0" + sG1;
						}
						if (sS1.length === 2) {
							sS1 = "0" + sS1;
						} 
						else if (sS1.length == 1) {
							sS1 = "00" + sS1;
						}
						if (sP1.length === 1) {
							sP1 = "0" + sP1;
						}
						if (sG2.length === 1) {
							sG2 = "0" + sG2;
						}
						if (sS2.length === 2) {
							sS2 = "0" + sS2;
						} 
						else if (sS2.length == 1) {
							sS2 = "00" + sS2;
						}
						if (sP2.length === 1) {
							sP2 = "0" + sP2;
						}
						sCoords1 = sG1 + sS1 + sP1;
						sCoords2 = sG2 + sS2 + sP2;
						return sCoords1 > sCoords2 ? 1 : -1;
					}), 
					new OGameNX.helperClasses.SortOrder("POSITION", "DESC", "PLANETLIST_OPTION_ORDERS_POSITION_DESC", function(a, b) {
						var /**@type String*/ sCoords1 = $(".planet-koords", $(a)).text().replace(/[\[\]\s]/g, '');
						var /**@type String*/ sCoords2 = $(".planet-koords", $(b)).text().replace(/[\[\]\s]/g, '');
						var /**@type RegExp*/ oRegExpCoords = /(\d{1,2}):(\d{1,3}):(\d{1,2})/g;
						var /**@type String*/ sG1 = sCoords1.replace(oRegExpCoords, "$1");
						var /**@type String*/ sS1 = sCoords1.replace(oRegExpCoords, "$2");
						var /**@type String*/ sP1 = sCoords1.replace(oRegExpCoords, "$3");
						var /**@type String*/ sG2 = sCoords2.replace(oRegExpCoords, "$1");
						var /**@type String*/ sS2 = sCoords2.replace(oRegExpCoords, "$2");
						var /**@type String*/ sP2 = sCoords2.replace(oRegExpCoords, "$3");
						if (sG1.length === 1) {
							sG1 = "0" + sG1;
						}
						if (sS1.length === 2) {
							sS1 = "0" + sS1;
						} 
						else if (sS1.length == 1) {
							sS1 = "00" + sS1;
						}
						if (sP1.length === 1) {
							sP1 = "0" + sP1;
						}
						if (sG2.length === 1) {
							sG2 = "0" + sG2;
						}
						if (sS2.length === 2) {
							sS2 = "0" + sS2;
						} 
						else if (sS2.length == 1) {
							sS2 = "00" + sS2;
						}
						if (sP2.length === 1) {
							sP2 = "0" + sP2;
						}
						sCoords1 = sG1 + sS1 + sP1;
						sCoords2 = sG2 + sS2 + sP2;
						return sCoords1 > sCoords2 ? -1 : 1;
					}), 
					new OGameNX.helperClasses.SortOrder("SIZE", "ASC", "PLANETLIST_OPTION_ORDERS_DIAMETER_ASC", function(a, b) {
						var /** @type Number*/ nSize1 = parseInt($(".planetlink", a).attr("ogamenx-diameter"), 10);
						var /** @type Number*/ nSize2 = parseInt($(".planetlink", b).attr("ogamenx-diameter"), 10);
						return nSize1 > nSize2 ? 1 : -1;
					}),
					new OGameNX.helperClasses.SortOrder("SIZE", "DESC", "PLANETLIST_OPTION_ORDERS_DIAMETER_DESC", function(a, b) {
						var /** @type Number*/ nSize1 = parseInt($(".planetlink", a).attr("ogamenx-diameter"), 10);
						var /** @type Number*/ nSize2 = parseInt($(".planetlink", b).attr("ogamenx-diameter"), 10);
						return nSize1 > nSize2 ? -1 : 1;
					}),
					new OGameNX.helperClasses.SortOrder("SIZE", "ASC", "PLANETLIST_OPTION_ORDERS_MOONDIAMETER_ASC", function(a, b) {
						var /** @type Number*/ nSize1 = parseInt($(".moonlink", a).attr("ogamenx-diameter"), 10);
						var /** @type Number*/ nSize2 = parseInt($(".moonlink", b).attr("ogamenx-diameter"), 10);
						return nSize1 > nSize2 ? 1 : -1;
					}),
					new OGameNX.helperClasses.SortOrder("SIZE", "DESC", "PLANETLIST_OPTION_ORDERS_MOONDIAMETER_DESC", function(a, b) {
						var /** @type Number*/ nSize1 = parseInt($(".moonlink", a).attr("ogamenx-diameter"), 10);
						var /** @type Number*/ nSize2 = parseInt($(".moonlink", b).attr("ogamenx-diameter"), 10);
						return nSize1 > nSize2 ? -1 : 1;
					}),
					new OGameNX.helperClasses.SortOrder("SIZE", "ASC", "PLANETLIST_OPTION_ORDERS_TEMPERATURELOW_ASC", function(a, b) {
						var /** @type Number*/ nMinTemp1 = parseInt($(".planetlink", a).attr("ogamenx-min-temperature"), 10);
						var /** @type Number*/ nMinTemp2 = parseInt($(".planetlink", b).attr("ogamenx-min-temperature"), 10);
						return nMinTemp1 > nMinTemp2 ? 1 : -1;
					}),
					new OGameNX.helperClasses.SortOrder("SIZE", "DESC", "PLANETLIST_OPTION_ORDERS_TEMPERATURELOW_DESC", function(a, b) {
						var /** @type Number*/ nMinTemp1 = parseInt($(".planetlink", a).attr("ogamenx-min-temperature"), 10);
						var /** @type Number*/ nMinTemp2 = parseInt($(".planetlink", b).attr("ogamenx-min-temperature"), 10);
						return nMinTemp1 > nMinTemp2 ? -1 : 1;
					}),
					new OGameNX.helperClasses.SortOrder("SIZE", "ASC", "PLANETLIST_OPTION_ORDERS_MOONTEMPERATUREHIGH_ASC", function(a, b) {
						var /** @type Number*/ nMaxTemp1 = parseInt($(".moonlink", a).attr("ogamenx-max-temperature"), 10);
						var /** @type Number*/ nMaxTemp2 = parseInt($(".moonlink", b).attr("ogamenx-max-temperature"), 10);
						return nMaxTemp1 > nMaxTemp2 ? 1 : -1;
					}), 
					new OGameNX.helperClasses.SortOrder("SIZE", "DESC", "PLANETLIST_OPTION_ORDERS_MOONTEMPERATUREHIGH_DESC", function(a, b) {
						var /** @type Number*/ nMaxTemp1 = parseInt($(".moonlink", a).attr("ogamenx-max-temperature"), 10);
						var /** @type Number*/ nMaxTemp2 = parseInt($(".moonlink", b).attr("ogamenx-max-temperature"), 10);
						return nMaxTemp1 > nMaxTemp2 ? -1 : 1;
					})
			    ];
			}
			return this['_orderCache'];
		}
	},

	/**
	 * Helper Classes (Wrapper Objects etc)
	 * @namespace
	 */
	helperClasses : {
		/**
		 * Wrapper Class for OGame NX Sort Orders
		 * Constructs a new SortOder Object
		 * @constructor
		 * @param {String} sOption Sort Option
		 * @param {String} sDirection "ASC" or "DESC" 
		 * @param {String} sDictKey Dictionary Key
		 * @param {Function} [fSortFunction=undefined] If defined the function will be uses as comparator function
		 */
		SortOrder : function(sOption, sDirection, sDictKey, fComparatorFunction) {
			return {
				getLabel : function(sLangCode) {
					return OGameNX.dictionary.getPhrase(sDictKey, sLangCode);
				},
				getDirection : function() {
					return sDirection;
				},
				getOption : function() {
					return sOption;
				},
				getComparatorFunction : function() {
					return (typeof (fComparatorFunction) === 'function') ? fComparatorFunction : undefined;

				}
			};

		}
	},

	/**
	 * Dictionary for messages/phrases that aren't covered within OGame itself
	 * @namespace
	 */
	dictionary : {
		/**
		 * Returns the requested phrase (by Dictionary Key) within the given language or <em>German</em> as fallback if no tranlsation is defined.
		 * <br/>A placeholder message is returned in case of an invalid Dictionary Key.
		 * @param {String} sDictKey Dictionary Key 
		 * @see OGameNX.dictionary.de
		 * @param {String} [sLangCode="{OGame's Ingame Language Setting}"] 
		 * @returns {String}
		 */
		getPhrase : function(sDictKey, sLangCode) {
			if (typeof (sLangCode) === 'undefined' || sLangCode.length === 0) {
				sLangCode = unsafeWindow.constants.language;
			}
			var /** @type String */
			sFallBackLangCode = "en";
			if (typeof (OGameNX.dictionary[sLangCode]) === 'undefined') {
				sLangCode = sFallBackLangCode;
			}
			var /** @type String */
			sResult = this[sFallBackLangCode].ERROR_INVALID_DICTKEY;
			if (typeof (OGameNX.dictionary[sLangCode][sDictKey]) === 'string') {
				sResult = OGameNX.dictionary[sLangCode][sDictKey];
			}
			sResult = sResult.replace(/\{ScriptName\}/g, GM_info.script.name);
			sResult = sResult.replace(/\{ScriptVersion\}/g, GM_info.script.version);
			sResult = sResult.replace(/\{ScriptVersion\}/g, GM_info.script.version);
			sResult = sResult.replace(/\{ScriptUpdateURL\}/g, GM_info.scriptMetaStr.replace(/[\s\S]*\/\/\s*@updateURL\s+(\S+)[\s\S]*/g, '$1'));
			sResult = sResult.replace(/\{DictionaryKey\}/g, (sDictKey + ''));
			return sResult;
		},

		/**
		 * English Messages
		 * These are used as fallbacks if no translation for the requested language is available
		 * @namespace
		 */
		en: {
			ERROR_PARAM_INVALID_TYPE : "The parameter does not have the Expected type.",
			ERROR_PARAM_EMPTY : "The parameter must not be empty",
			ERROR_PARAM_INVALID_FORMAT : "The parameter does not have the expected format.",
			ERROR_INVALID_DICTKEY : "There's no entry in the dictionary for the given key '{DictionaryKey}'.",

			INFO_NEWER_VERSION : 'A newer version of {ScriptName} is available. Click <a href="{ScriptUpdateUrl}" target="_blank">here</a> to update.',

			SCRIPT_TITLE : "{ScriptName}",
			SCRIPT_OPTION_ENABLEUDPATENOTIFICATION : "Show message if update is available",

			CONSTRUCTION_TITLE : "Construction",
			CONSTRUCTION_OPTION_SHOWPOSSIBLEBUILDBEGIN : "Show possible construction time",

			RESOURCEBAR_TITLE : "Resource-Bar",
			RESOURCEBAR_OPTION_SHOWSTORAGECAPACITY : "Show storage capacity",
			RESOURCEBAR_OPTION_SHOWREMAININGTIME : "Show remaining time until storages are full",

			MESSAGES_TITLE : "Messages",
			MESSAGES_OPTION_SHOWCONTENT : "Load message's content automatically",

			PLANETLIST_TITLE : "Planet-List",
			PLANETLIST_OPTION_SHOWSIZE : "Show planet's size",
			PLANETLIST_OPTION_SHOWTEMPERATURE : "Show min/max temperature of planet",
			PLANETLIST_OPTION_SHOWACTIVEMOONDATA : "Show active moon's data instead of planet's data",
			PLANETLIST_OPTION_SHOWREMAININGTIME : "Show remaining construction time",
			PLANETLIST_OPTION_ORDERBY : "Order by",
			PLANETLIST_OPTION_ORDERS_DEFAULT_ASC : "OGame default order",
			PLANETLIST_OPTION_ORDERS_DEFAULT_DESC : "OGame default order (reversed)",
			PLANETLIST_OPTION_ORDERS_NAME_ASC : "Planet's name (ascending)",
			PLANETLIST_OPTION_ORDERS_NAME_DESC : "Planet's name (descending)",
			PLANETLIST_OPTION_ORDERS_POSITION_ASC : "Planet's position (ascending)",
			PLANETLIST_OPTION_ORDERS_POSITION_DESC : "Planet's position (descending)",
			PLANETLIST_OPTION_ORDERS_DIAMETER_ASC : "Planet's diameter (ascending)",
			PLANETLIST_OPTION_ORDERS_DIAMETER_DESC : "Planet's diameter (descending)",
			PLANETLIST_OPTION_ORDERS_MOONDIAMETER_ASC : "Moon's diameter (ascending)",
			PLANETLIST_OPTION_ORDERS_MOONDIAMETER_DESC : "Moon's diameter (descending)",
			PLANETLIST_OPTION_ORDERS_TEMPERATURELOW_ASC : "Planet's minimum temperature (ascending)",
			PLANETLIST_OPTION_ORDERS_TEMPERATURELOW_DESC : "Planet's minimum temperature (descending)",
			PLANETLIST_OPTION_ORDERS_TEMPERATUREHIGH_ASC : "Planet's maximum temperature (ascending)",
			PLANETLIST_OPTION_ORDERS_TEMPERATUREHIGH_DESC : "Planet's maximum temperature (descending)",
			PLANETLIST_OPTION_ORDERS_MOONTEMPERATURELOW_ASC : "Moon's minimum temperature (ascending)",
			PLANETLIST_OPTION_ORDERS_MOONTEMPERATURELOW_DESC : "Moon's minimum temperature (descending)",
			PLANETLIST_OPTION_ORDERS_MOONTEMPERATUREHIGH_ASC : "Moon's maximum temperature (ascending)",
			PLANETLIST_OPTION_ORDERS_MOONTEMPERATUREHIGH_DESC : "Moon's maximum temperature (descending)",

			SAVE : "Save",
			CLOSE : "Close",
			RESET : "Reset",
			EXPERIMENTAL : 'Experimental'			
		},
		/**
		 * German Messages
		 * @namespace
		 */
		de : {
			ERROR_PARAM_INVALID_TYPE : "Der Paramter hat nicht den erwarteten Typ.",
			ERROR_PARAM_EMPTY : "Der Parameter darf nicht leer sein",
			ERROR_PARAM_INVALID_FORMAT : "Der Parameter hat nicht das erwartete Format",
			ERROR_INVALID_DICTKEY : "Es gib keinen Eintrag im W�rterbuch f�r den Schl�ssel '{DictionaryKey}'.",

			INFO_NEWER_VERSION : 'Eine aktualisierte Version von {ScriptName} ist verf�gbar. Klicken Sie <a href="{ScriptUpdateUrl}" target="_blank">hier</a> um das Update einzuspielen.',

			SCRIPT_TITLE : "{ScriptName}",
			SCRIPT_OPTION_ENABLEUDPATENOTIFICATION : "Benachrichtigung bei Update anzeigen",

			CONSTRUCTION_TITLE : "Konstruktion",
			CONSTRUCTION_OPTION_SHOWPOSSIBLEBEGIN : "M�glichen Baubeginn anzeigen",
			
			RESEARCH_TITLE : "Forschung",
			RESEARCH_OPTION_SHOWPOSSIBLEBEGIN : "M�glichen Forschungsbeginn anzeigen",

			RESOURCEBAR_TITLE : "Ressourcen-Leiste",
			RESOURCEBAR_OPTION_SHOWSTORAGECAPACITY : "Lagerkapazit�t anzeigen",
			RESOURCEBAR_OPTION_SHOWREMAININGTIME : "Restzeit anzeigen bis die jeweiligen Lager voll sind",

			MESSAGES_TITLE : "Nachrichten",
			MESSAGES_OPTION_SHOWCONTENT : "Nachrichteninhalt automatisch laden",

			PLANETLIST_TITLE : "Planetenliste",
			PLANETLIST_OPTION_SHOWSIZE : "Planetengr��e anzeigen",
			PLANETLIST_OPTION_SHOWTEMPERATURE : "Temperaturbereich anzeigen",
			PLANETLIST_OPTION_SHOWACTIVEMOONDATA : "Daten des aktive Mondes, statt des Planeten zeigen",
			PLANETLIST_OPTION_SHOWREMAININGTIME : "Verbleibende Geb�udebauzeit anzeigen",
			PLANETLIST_OPTION_ORDERBY : "Sortieren nach",
			PLANETLIST_OPTION_ORDERS_DEFAULT_ASC : "OGame Standardsortierung",
			PLANETLIST_OPTION_ORDERS_DEFAULT_DESC : "OGame Standardsortierung (umgekehrt)",
			PLANETLIST_OPTION_ORDERS_NAME_ASC : "Nach Name des Planeten (aufsteigend)",
			PLANETLIST_OPTION_ORDERS_NAME_DESC : "Nach Name des Planeten (absteigend)",
			PLANETLIST_OPTION_ORDERS_POSITION_ASC : "Nach Position des Planeten (aufsteigend)",
			PLANETLIST_OPTION_ORDERS_POSITION_DESC : "Nach Position des Planeten (absteigend)",
			PLANETLIST_OPTION_ORDERS_DIAMETER_ASC : "Nach Durchmesser des Planeten (aufsteigend)",
			PLANETLIST_OPTION_ORDERS_DIAMETER_DESC : "Nach Durchmesser des Planeten (absteigend)",
			PLANETLIST_OPTION_ORDERS_MOONDIAMETER_ASC : "Nach Durchmesser des Mondes (aufsteigend)",
			PLANETLIST_OPTION_ORDERS_MOONDIAMETER_DESC : "Nach Durchmesser des Mondes (absteigend)",
			PLANETLIST_OPTION_ORDERS_TEMPERATURELOW_ASC : "Nach Minimaltemperatur des Planeten (aufsteigend)",
			PLANETLIST_OPTION_ORDERS_TEMPERATURELOW_DESC : "Nach Minimaltemperatur des Planeten (absteigend)",
			PLANETLIST_OPTION_ORDERS_TEMPERATUREHIGH_ASC : "Nach Maximaltemperatur des Planeten (aufsteigend)",
			PLANETLIST_OPTION_ORDERS_TEMPERATUREHIGH_DESC : "Nach Maximaltemperatur des Planeten (absteigend)",
			PLANETLIST_OPTION_ORDERS_MOONTEMPERATURELOW_ASC : "Nach Minimaltemperatur des Mondes (aufsteigend)",
			PLANETLIST_OPTION_ORDERS_MOONTEMPERATURELOW_DESC : "Nach Minimaltemperatur des Mondes (absteigend)",
			PLANETLIST_OPTION_ORDERS_MOONTEMPERATUREHIGH_ASC : "Nach Maximaltemperatur des Mondes (aufsteigend)",
			PLANETLIST_OPTION_ORDERS_MOONTEMPERATUREHIGH_DESC : "Nach Maximaltemperatur des Mondes (absteigend)",

			SAVE : "Speichern",
			CLOSE : "Schlie�en",
			RESET : "Zur�cksetzen",
			EXPERIMENTAL : 'Experimentell'
		}
	}
};

///////////////////////////////////////// JQuery Extensions /////////////////////////////////////////

/**
 * jQuery.fn.sortElements
 * --------------
 * @param Function comparator:
 *   Exactly the same behaviour as [1,2,3].sort(comparator)
 *   
 * @param Function getSortable
 *   A function that should return the element that is
 *   to be sorted. The comparator will run on the
 *   current collection, but you may want the actual
 *   resulting sort to occur on a parent or another
 *   associated element.
 *   
 *   E.g. $('td').sortElements(comparator, function(){
 *      return this.parentNode; 
 *   })
 *   
 *   The <td>'s parent (<tr>) will be sorted instead
 *   of the <td> itself.
 */
jQuery.fn.sortElements = (function() {
	var sort = [].sort;
	return function(comparator, getSortable) {
		getSortable = getSortable || function() {
			return this;
		};
		var placements = this.map(function() {
			var sortElement = getSortable.call(this), parentNode = sortElement.parentNode,
			// Since the element itself will change position, we have
			// to have some way of storing its original position in
			// the DOM. The easiest way is to have a 'flag' node:
			nextSibling = parentNode.insertBefore(document.createTextNode(''), sortElement.nextSibling);
			return function() {
				if (parentNode === this) {
					throw new Error("You can't sort elements if any one is a descendant of another.");
				}
				// Insert before flag:
				parentNode.insertBefore(this, nextSibling);
				// Remove flag:
				parentNode.removeChild(nextSibling);
			};
		});
		return sort.call(this, comparator).each(function(i) {
			placements[i].call(getSortable.call(this));
		});
	};
})();